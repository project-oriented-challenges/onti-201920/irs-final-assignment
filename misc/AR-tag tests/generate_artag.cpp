#include <iostream>
#include <math.h>
#include <stack>
#include <string>
using namespace std;


class ArTag
{
public:
    static const int size = 33;
    int a[size]; //numeration starts from 1

    void encode(int * commands, int n)
    {
        int h = 0;
        for (int i = 1; i<size;i++)
        {
            if (log2(i)-int(log2(i)) == 0)
                a[i]=0;//hit a control bit
            else
            {
                if (h<n)
                {
                    switch (*(commands + h))
                    {
                        case 0:
                            a[i] = 0;
                            if (log2(i+1)-int(log2(i+1)) != 0)
                                a[i + 1] = 0;
                            else
                            {
                                a[i + 1] = 0;
                                a[i + 2] = 0;
                                i++;
                            }
                            break;
                        case 1:
                            a[i] = 0;
                            if (log2(i+1)-int(log2(i+1)) != 0)
                                a[i + 1] = 1;
                            else
                            {
                                a[i + 1] = 0;
                                a[i + 2] = 1;
                                i++;
                            }
                            break;
                        case 2:
                            a[i] = 1;
                            if (log2(i+1)-int(log2(i+1)) != 0)
                                a[i + 1] = 0;
                            else
                            {
                                a[i + 1] = 0;
                                a[i + 2] = 0;
                                i++;
                            }
                            break;
                        case 3:
                            a[i] = 1;
                            if (log2(i+1)-int(log2(i+1)) != 0)
                                a[i + 1] = 1;
                            else
                            {
                                a[i + 1] = 0;
                                a[i + 2] = 1;
                                i++;
                            }
                            break;
                    }
                }
                else
                {
                    a[i]=0;
                    a[i+1] =0;
                }
                    i++;
                    h += 1;

            }
        }
        for (int i = 1; i<size;i++)
        {
            if (log2(i)-int(log2(i)) == 0)
            {
                int controlledBits = 0;
                int sum = 0;
                for (int j = i; j<size; j++)
                {
                    if (controlledBits<=i)
                    {
                        sum+=a[j];
                        controlledBits++;
                    }
                    if (controlledBits == i)
                    {
                        controlledBits=0;
                        j+=i;
                    }
                }
                if (sum%2==0)
                    a[i]=0;
                else
                    a[i]=1;
            }
            else continue;
        }
    }
};


int main() {
    //freopen("input.txt", "r", stdin);
    freopen("artags.txt","w", stdout);
    int a[13];
    srand (time(nullptr));

    for (int j=0;j<25;j++)
    {
        for (int i = 0; i < 13; i++) {
            a[i] = rand() % 4;
            cout<<a[i]<<" ";
        }
        cout<<endl;

        ArTag ar;
        ar.encode(a, 13);
        for (int i = 1; i < ar.size; i++) {
            cout << ar.a[i];
        }
        cout<<endl<<endl;

    }
    return 0;
}

