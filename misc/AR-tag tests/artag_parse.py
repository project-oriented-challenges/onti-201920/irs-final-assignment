import base64
import numpy as np




for i in range(25):
    infile = "artag" + str(i) + ".txt"
    outfile = "" + str(i)
    infile2 = "Solution_ARTAG" + str(i) + ".txt"
    outfile2 = "" + str(i) + ".clue"


    infile = open(infile, 'r')
    infile2 = open(infile2, 'r')
    outfile = open(outfile, 'w')
    outfile2 = open(outfile2, 'w')
    
    indata = base64.b64decode(infile.read().strip() + "========")

    if len(indata) == 230400:
        IMAGE_SIZE = (240, 320)
    else:
        IMAGE_SIZE = (120, 160)

    def chunks(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    s = ' '.join([ ''.join(p) for p in chunks([ '%02X' % x for x in indata ], 3) ])

    outfile.write(s)
    outfile2.write(infile2.read())
