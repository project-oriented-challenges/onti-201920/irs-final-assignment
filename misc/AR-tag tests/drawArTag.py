from PIL import Image, ImageDraw

ARTAG_SIZE = 8
ARTAG_SQUARE_SIZE = 25
ARTAG_MAIN_COLOR = "black"
ARTAG_SUB_COLOR = "white"

f = open("artags.txt")
s = f.readlines()
counter = 0
for line in s:
    im = Image.new('RGB', (ARTAG_SIZE * ARTAG_SQUARE_SIZE, ARTAG_SIZE * ARTAG_SQUARE_SIZE))
    draw = ImageDraw.Draw(im)
    
    
    for i in range(0,ARTAG_SIZE): #this cycle draws border of ARTAG
        draw.rectangle((i*ARTAG_SQUARE_SIZE, 0, (i+1)*ARTAG_SQUARE_SIZE-1, ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR)
        draw.rectangle((0, i*ARTAG_SQUARE_SIZE, ARTAG_SQUARE_SIZE-1, (i+1)*ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR)
        draw.rectangle(((ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE, i*ARTAG_SQUARE_SIZE, ARTAG_SIZE*ARTAG_SQUARE_SIZE-1, (i+1)*ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR) 
        draw.rectangle((i*ARTAG_SQUARE_SIZE, (ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE, (i+1)*ARTAG_SQUARE_SIZE-1, ARTAG_SIZE*ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR)
            
            
    i = 0
    flag = line=="\n\r" or line=="\n" or line=="\r"
    for c in line:
        if (c=='0' or c =='1' or c=='\n' or c =='\r'):
            i+=1
        else:
            flag = True;
        offset_x = 0
        offset_y = 0
        color = ARTAG_MAIN_COLOR if c=='0' else ARTAG_SUB_COLOR #Note that 0 is BLACK and 1 is WHITE
        
        if (i>0 and i<=ARTAG_SIZE-4): #upper row
            offset_x = (i+1)*ARTAG_SQUARE_SIZE
            offset_y = ARTAG_SQUARE_SIZE
            
        if (i>ARTAG_SIZE-4 and i<=(ARTAG_SIZE-4)+(ARTAG_SIZE-2)*(ARTAG_SIZE-4)): #middle rows
            offset_x = (((i -(ARTAG_SIZE-4)-1) %(ARTAG_SIZE-2))+1) * ARTAG_SQUARE_SIZE
            offset_y = ((i - (ARTAG_SIZE-4)-1)//(ARTAG_SIZE-2) + 2) * ARTAG_SQUARE_SIZE
            
        if (i>(ARTAG_SIZE-4)+(ARTAG_SIZE-2)*(ARTAG_SIZE-4) and i<=(ARTAG_SIZE-4)*2+(ARTAG_SIZE-2)*(ARTAG_SIZE-4)):#last row   
            offset_x = (i-((ARTAG_SIZE-4)+(ARTAG_SIZE-2)*(ARTAG_SIZE-4))+1)*ARTAG_SQUARE_SIZE
            offset_y = (ARTAG_SIZE-2)*ARTAG_SQUARE_SIZE
        
        if (offset_x!=0 and offset_y!=0):    
            draw.rectangle((offset_x, offset_y, offset_x + ARTAG_SQUARE_SIZE-1, offset_y + ARTAG_SQUARE_SIZE-1), fill = color)
            
    
    draw.rectangle(((ARTAG_SIZE-2)*ARTAG_SQUARE_SIZE, (ARTAG_SIZE-2)*ARTAG_SQUARE_SIZE, (ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE-1, (ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE-1), fill=ARTAG_SUB_COLOR) #bottom right corner
    
    draw.rectangle((ARTAG_SQUARE_SIZE, ARTAG_SQUARE_SIZE, 2*ARTAG_SQUARE_SIZE-1, 2*ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR) #upper left corner
    
    draw.rectangle((ARTAG_SQUARE_SIZE, (ARTAG_SIZE-2)*ARTAG_SQUARE_SIZE, 2*ARTAG_SQUARE_SIZE-1, (ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE-1), fill=ARTAG_MAIN_COLOR) #bottom left corner
    
    draw.rectangle(((ARTAG_SIZE-2)*(ARTAG_SQUARE_SIZE), ARTAG_SQUARE_SIZE, (ARTAG_SIZE-1)*ARTAG_SQUARE_SIZE, 2*ARTAG_SQUARE_SIZE), fill=ARTAG_MAIN_COLOR) #upper right corner
    
    if (not flag):
        with open("ARTAG" + str(counter) + ".png", 'wb') as f:
            im.save(f)
        counter+=1
    else:
        with open("Solution_ARTAG" + str(counter) + ".txt", 'w') as f:
            if not(line=="\n\r" or line=="\n" or line=="\r"):
                f.write(line)
    
   # im.show()


