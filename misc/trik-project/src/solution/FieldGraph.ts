import { copy } from './helpers';

/** - The number of free cells at each direction:
 *     - `r_free:` right
 *     - `l_free:` left
 *     - `f_free:` forward
 *     - `b_free:` back
 *
 *  - The number of free cells before wall at each direction (if wall detected):
 *     - `r_wall`, `l_wall`, `f_wall`, `b_wall`
 */
export interface WallsState {
    // TODO: optimize localization using new walls
    // TODO: add better graph walking algorithm
    // TODO: add better localization algorithm
    r_free: number;
    l_free: number;
    f_free: number;
    b_free: number;

    r_wall?: number;
    l_wall?: number;
    f_wall?: number;
    b_wall?: number;
}

export enum CellState {
    Unknown = 0,
    Wall,
    Free,
}

/**
 * The direction of the robot:
 *   - `0`: right
 *   - `1`: up
 *   - `2`: left
 *   - `3`: bottom
 */
export enum Direction {
    Unknown = 100,
    Right = 0,
    Up = 1,
    Left = 2,
    Down = 3,
}

export function negateDirection(dir: Direction) {
    switch (dir) {
        case Direction.Unknown:
            return Direction.Unknown;
        case Direction.Right:
            return Direction.Left;
        case Direction.Left:
            return Direction.Right;
        case Direction.Up:
            return Direction.Down;
        case Direction.Down:
            return Direction.Up;
    }
}

export type Node = [number, number];

export const FIELD_SIZE = 8;

export class FieldGraph {
    public field: CellState[][] = [];

    get length(): number {
        return this.field.length;
    }

    constructor() {
        for (let i = 0; i < FIELD_SIZE * 3; ++i) {
            this.field.push([]);
            for (let j = 0; j < FIELD_SIZE * 3; ++j) {
                this.field[i].push(CellState.Unknown);
            }
        }
    }

    private *clearWallsVertical(
        [i, j]: Node,
        di_start: number,
        di_end: number
    ): Iterable<Node> {
        for (let di = di_start; di <= di_end; ++di) {
            this.updateCell(i + di, j, CellState.Free);
            yield [i + di, j];
        }
    }

    private *clearWallsHorizontal(
        [i, j]: Node,
        dj_start: number,
        dj_end: number
    ): Iterable<Node> {
        for (let dj = dj_start; dj <= dj_end; ++dj) {
            this.updateCell(i, j + dj, CellState.Free);
            yield [i, j + dj];
        }
    }

    private updateCell(i: number, j: number, val: CellState) {
        if (this.field[i][j] !== CellState.Unknown && this.field[i][j] !== val)
            throw new RangeError(`Updated scanned cell: ${[i, j]}, ${val}`);
        this.field[i][j] = val;
    }

    /**
     * Yields free nodes (for localization)
     */
    public *updateField(
        node: Node,
        walls: WallsState,
        direction: number
    ): Iterable<Node> {
        const [i, j] = node;

        switch (direction) {
            case 0:
                if (walls.l_free === 0)
                    this.updateCell(i - 1, j, CellState.Wall);
                if (walls.f_free === 0)
                    this.updateCell(i, j + 1, CellState.Wall);
                if (walls.b_free === 0)
                    this.updateCell(i, j - 1, CellState.Wall);
                if (walls.r_free === 0)
                    this.updateCell(i + 1, j, CellState.Wall);

                if (walls.l_wall !== undefined)
                    this.updateCell(i - 1 - walls.l_wall, j, CellState.Wall);
                if (walls.f_wall !== undefined)
                    this.updateCell(i, j + 1 + walls.f_wall, CellState.Wall);
                if (walls.b_wall !== undefined)
                    this.updateCell(i, j - 1 - walls.b_wall, CellState.Wall);
                if (walls.r_wall !== undefined)
                    this.updateCell(i + 1 + walls.r_wall, j, CellState.Wall);

                yield* this.clearWallsVertical(
                    node,
                    -walls.l_free,
                    walls.r_free
                );
                yield* this.clearWallsHorizontal(
                    node,
                    -walls.b_free,
                    walls.f_free
                );
                break;
            case 1:
                if (walls.l_free === 0)
                    this.updateCell(i, j - 1, CellState.Wall);
                if (walls.f_free === 0)
                    this.updateCell(i - 1, j, CellState.Wall);
                if (walls.b_free === 0)
                    this.updateCell(i + 1, j, CellState.Wall);
                if (walls.r_free === 0)
                    this.updateCell(i, j + 1, CellState.Wall);

                if (walls.l_wall !== undefined)
                    this.updateCell(i, j - 1 - walls.l_wall, CellState.Wall);
                if (walls.f_wall !== undefined)
                    this.updateCell(i - 1 - walls.f_wall, j, CellState.Wall);
                if (walls.b_wall !== undefined)
                    this.updateCell(i + 1 + walls.b_wall, j, CellState.Wall);
                if (walls.r_wall !== undefined)
                    this.updateCell(i, j + 1 + walls.r_wall, CellState.Wall);

                yield* this.clearWallsVertical(
                    node,
                    -walls.f_free,
                    walls.b_free
                );
                yield* this.clearWallsHorizontal(
                    node,
                    -walls.l_free,
                    walls.r_free
                );
                break;
            case 2:
                if (walls.l_free === 0)
                    this.updateCell(i + 1, j, CellState.Wall);
                if (walls.f_free === 0)
                    this.updateCell(i, j - 1, CellState.Wall);
                if (walls.b_free === 0)
                    this.updateCell(i, j + 1, CellState.Wall);
                if (walls.r_free === 0)
                    this.updateCell(i - 1, j, CellState.Wall);

                if (walls.l_wall !== undefined)
                    this.updateCell(i + 1 + walls.l_wall, j, CellState.Wall);
                if (walls.f_wall !== undefined)
                    this.updateCell(i, j - 1 - walls.f_wall, CellState.Wall);
                if (walls.b_wall !== undefined)
                    this.updateCell(i, j + 1 + walls.b_wall, CellState.Wall);
                if (walls.r_wall !== undefined)
                    this.updateCell(i - 1 - walls.r_wall, j, CellState.Wall);

                yield* this.clearWallsVertical(
                    node,
                    -walls.r_free,
                    walls.l_free
                );
                yield* this.clearWallsHorizontal(
                    node,
                    -walls.f_free,
                    walls.b_free
                );
                break;
            case 3:
                if (walls.l_free === 0)
                    this.updateCell(i, j + 1, CellState.Wall);
                if (walls.f_free === 0)
                    this.updateCell(i + 1, j, CellState.Wall);
                if (walls.b_free === 0)
                    this.updateCell(i - 1, j, CellState.Wall);
                if (walls.r_free === 0)
                    this.updateCell(i, j - 1, CellState.Wall);

                if (walls.l_wall !== undefined)
                    this.updateCell(i, j + 1 + walls.l_wall, CellState.Wall);
                if (walls.f_wall !== undefined)
                    this.updateCell(i + 1 + walls.f_wall, j, CellState.Wall);
                if (walls.b_wall !== undefined)
                    this.updateCell(i - 1 - walls.b_wall, j, CellState.Wall);
                if (walls.r_wall !== undefined)
                    this.updateCell(i, j - 1 - walls.r_wall, CellState.Wall);

                yield* this.clearWallsVertical(
                    node,
                    -walls.b_free,
                    walls.f_free
                );
                yield* this.clearWallsHorizontal(
                    node,
                    -walls.r_free,
                    walls.l_free
                );
                break;
        }
    }

    public relativePosition(of: Node, to: Node): Direction {
        const [i0, j0] = to;
        const [i1, j1] = of;

        if (i1 === i0 + 1 && j1 === j0) return Direction.Down;
        if (i1 === i0 - 1 && j1 === j0) return Direction.Up;
        if (j1 === j0 + 1 && i1 === i0) return Direction.Right;
        if (j1 === j0 - 1 && i1 === i0) return Direction.Left;

        return Direction.Unknown;
    }

    /**
     * Return reachable neighbor nodes
     */
    public *neighbors(
        [i, j]: Node,
        allowUnknown: boolean = false
    ): Iterable<Node> {
        const allowedVals = [CellState.Free];
        if (allowUnknown) allowedVals.push(CellState.Unknown);
        yield* ([
            [i - 1, j],
            [i, j - 1],
            [i + 1, j],
            [i, j + 1],
        ] as Node[]).filter(([i_, j_]) =>
            i_ < 0 || j_ < 0 || i_ >= this.length || j_ >= this.length
                ? false
                : allowedVals.includes(this.field[i_][j_])
        );
    }

    public *dfs(
        startNode: Node,
        allowUnknown: boolean = false
    ): Iterable<Node> {
        const stack = [startNode];

        // List are just links, so we can't use list as a hashable type in JS
        const visited: { [node: string]: boolean } = {};

        while (stack.length > 0) {
            const [i, j] = stack.pop()!;
            if (`${i}:${j}` in visited) {
                continue;
            }

            yield [i, j];
            visited[`${i}:${j}`] = true;

            stack.push(...this.neighbors([i, j], allowUnknown));
        }
    }

    private _bfs_path(
        startNode: Node,
        endNode: Node,
        allowUnknown: boolean = false
    ): Node[] | undefined {
        const q = [[startNode]];
        const visited: { [node: string]: boolean } = {};

        while (q.length > 0) {
            const path = q.shift()!;
            const [i, j] = path[path.length - 1];

            if (`${i}:${j}` in visited) continue;
            visited[`${i}:${j}`] = true;

            if (i === endNode[0] && j === endNode[1]) {
                return path;
            }

            q.push(
                ...[...this.neighbors([i, j], allowUnknown)].map((neighbor) =>
                    copy(path).concat([neighbor])
                )
            );
        }

        return undefined;
    }

    public known_path_part(path: Node[]): Node[] {
        const ret: Node[] = [];
        for (const [i, j] of path) {
            if (this.field[i][j] !== CellState.Unknown) {
                ret.push([i, j]);
            } else {
                break;
            }
        }

        return ret;
    }

    public bfs_path(
        startNode: Node,
        endNode: Node
    ): { path: Node[] | undefined; known: boolean } {
        const p1 = this._bfs_path(startNode, endNode, false);
        if (p1 !== undefined) return { path: p1, known: true };

        return {
            path: this._bfs_path(startNode, endNode, true),
            known: false,
        };
    }

    public toString(): string {
        return this.field.map((line) => line.join(', ')).join('\n');
    }
}
