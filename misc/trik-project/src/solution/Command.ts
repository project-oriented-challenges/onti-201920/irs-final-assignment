export const enum Command {
    None = 0,
    TurnLeft = 1,
    TurnRight = 2,
    MoveForward = 3,
}
