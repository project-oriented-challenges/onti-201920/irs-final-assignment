
import ndarray_mod from "ndarray";

interface PrivateOptions {
    width: number;
}

interface Options {
    width?: number;
}


function showf(float: number, width:number): string {
    let val = float.toString();
    if (val.length > width)
        val = val.slice(0, width);
    val = " ".repeat(width - val.length) + val;
    return val;
}

function d1(m: NdArray, opts: PrivateOptions): string {
    let terms: string[] = [];
    for (var i = 0; i < m.shape[0]; i++) {
        terms.push(showf(m.get(i), opts.width));
    }
    return terms.join(' ');
}

function d2(m: NdArray, opts: PrivateOptions): string {
    var rows: string[] = [];
    for (var y = 0; y < m.shape[0]; y++) {
        rows.push(d1(m.pick(y, null), opts));
    }
    return rows.join('\n');
}

function d3(m: NdArray, opts: PrivateOptions): string {
    var rows: string[] = [];
    for (var z = 0; z < m.shape[0]; z++) {
        rows.push(d2(m.pick(z, null, null), opts), '');
    }
    return rows.join('\n');
}

function d4(m: NdArray, opts: PrivateOptions): string {
    var rows: string[] = [], len = 3
    for (var w = 0; w < m.shape[0]; w++) {
        var r = d3(m.pick(w, null, null, null), opts)
        rows.push(r);
        var lines = r.split('\n');
        for (var i = 0; i < lines.length; i++) {
            len = Math.max(len, lines[i].length);
        }
    }
    return rows.join('\n' + Array(len+1).join('-') + '\n\n');
}

function show(m: NdArray | Data, opts?: Options | number): string {
    if (!opts) opts = {};
    if (typeof opts === 'number') opts = { width: opts };
    if (!opts.width) opts.width = 8;
    const privateOpts: PrivateOptions = { width: opts.width };

    let x: NdArray;
    if ((<NdArray>m).dimension === undefined) {
        x = ndarray_mod(<Data>m);
    } else {
        x = <NdArray>m;
    }

    if (x.dimension === 1) return d1(x, privateOpts);
    if (x.dimension === 2) return d2(x, privateOpts);
    if (x.dimension === 3) return d3(x, privateOpts);
    if (x.dimension === 4) return d4(x, privateOpts);

    return "";
};

export default show;
