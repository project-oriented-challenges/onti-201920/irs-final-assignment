import { ARTAG_SIZE } from './libmorat-ng';
import { Command } from '../Command';
import { decode } from './hamming';

function decode_to_number(bin_artag: NdArray) {
    let ret = 0;
    let c = 0;
    for (let i = 1; i < ARTAG_SIZE - 1; ++i) {
        for (let j = 1; j < ARTAG_SIZE - 1; ++j) {
            if (
                (i === 1 && j === 1) ||
                (i === 1 && j === ARTAG_SIZE - 2) ||
                (i === ARTAG_SIZE - 2 && j === 1) ||
                (i === ARTAG_SIZE - 2 && j === ARTAG_SIZE - 2)
            ) {
                continue;
            }
            ret |= bin_artag.get(i, j) << c;
            c++;
        }
    }

    return ret;
}

export function artag_decode(bin_artag: NdArray): Command[] {
    let num = decode_to_number(bin_artag);
    num = decode(num);
    const commands = Array(13)
        .fill(0)
        .map(
            (_, i) => (((num >> (i * 2)) & 1) << 1) | ((num >> (i * 2 + 1)) & 1)
        );
    return commands as Command[];
}
