import ndarray from "ndarray";

export const ARTAG_SIZE = 8;

const SHAPE: [number, number] = [120, 160];
const LIGHT_NORM: [number, number] = [0.8741, -16.94];
const DARK_NORM: [number, number] = [1, 0];
const XDARK_NORM: [number, number] = [1.582, 0];

const MAP: [number, number][] = new Array(SHAPE[1]); 
MAP.fill(LIGHT_NORM);
MAP[3] = XDARK_NORM;
for (let i = 5; i < SHAPE[1]; i+= 2)
    MAP[i] = DARK_NORM;
MAP[7] = XDARK_NORM;

type pos = [number, number];

export class InvalidArtagImageError extends Error {
}

function filter(image: NdArray) {
    for (let j = 0; j < image.shape[1]; j++) {
        const [slope, intercept] = MAP[j];
        for (let i = 0; i < image.shape[0]; i++) {
            let v = slope * image.get(i, j) + intercept;
            v = Math.max(0, v);
            v = Math.min(255, v);
            image.set(i, j, v);
        }
    }
    for (let j = 0; j < image.shape[1]; j += 2) {
        for (let i = 0; i < image.shape[0]; i++) {
            let v = image.get(i, j) + image.get(i, j + 1);
            v /= 2;
            image.set(i, j, v | 0);
            image.set(i, j + 1, v | 0);
        }
    }
}

function histogram(image: NdArray): number[] {
    const res: number[] = new Array(256);
    res.fill(0);
    for (let i = 0; i < image.shape[0]; i++) { 
        for (let j = 0; j < image.shape[1]; j++)  {
            const v = image.get(i, j);
            res[v]++;
        }
    }
    return res;
}

function threshhold(image: NdArray, thresh: number) {
    for (let i = 0; i < image.shape[0]; i++) { 
        for (let j = 0; j < image.shape[1]; j++)  {
            const v = image.get(i, j);
            const nv = (v < thresh) ? 0 : 255;
            image.set(i, j, nv);
        }
    }
}

function otsu(image: NdArray) {
    /* http://www.labbookpages.co.uk/software/imgProc/otsuThreshold.html */
    const total = image.shape[0] * image.shape[1];
    const hist = histogram(image);

    const hist_sum = hist.reduce((a, x, i) => i * x + a, 0);
    let wB = 0, wF = 0, sumB = 0;
    let best_thresh = 0;
    let best_thresh_variance = 0;
    for (let i = 0; i < 256; i++) {
        wB += hist[i];
        if (wB == 0) continue;

        wF = total - wB;
        if (wF == 0) break;

        sumB += i * hist[i];

        const mB = sumB / wB;
        const mF = (hist_sum - sumB) / wF;
        const variance = wB * wF * (mB - mF) * (mB - mF);
        if (variance > best_thresh_variance) {
            best_thresh = i;
            best_thresh_variance = variance;
        }
    }
    threshhold(image, best_thresh);
}

function get_ccs(image: NdArray): pos[][] {
    function* nei(p: pos): Iterable<[number, number]> {
        const [i, j] = p;
        if (i > 0)                  yield [i - 1, j];
        if (i < image.shape[0] - 1) yield [i + 1, j];
        if (j > 0)                  yield [i, j - 1];
        if (j < image.shape[1] - 1) yield [i, j + 1];
    }
    const seen = ndarray(new Array(image.shape[0] * image.shape[1]), image.shape);
    seen.data.fill(0);
    const ccs: pos[][] = [];
    function bfs(p: pos, cc: number) {
        if (seen.get(...p) !== 0)
            return;
        seen.set(...p, 1);
        const queue: pos[] = [ p ];
        while (queue.length > 0) {
            const p = queue.shift()!;
            ccs[cc].push(p);
            function put(i: number, j: number) {
                if (image.get(i, j) === 0 && seen.get(i, j) === 0) {
                    seen.set(i, j, 1);
                    queue.push([i,j]);
                }
            }
            //for (let x of nei(p)) {
            const [i, j] = p;
            if (i > 0)                  put(i - 1, j);
            if (i < image.shape[0] - 1) put(i + 1, j);
            if (j > 0)                  put(i, j - 1);
            if (j < image.shape[1] - 1) put(i, j + 1);
            //}
        }
    }
    let cc = 0;
    for (let i = 0; i < image.shape[0]; i++) { 
        for (let j = 0; j < image.shape[1]; j++)  {
            const p: pos = [i, j];
            if (image.get(i, j) < 128 && seen.get(i, j) == 0) {
                console.log("bfs(" + p + ")");
                ccs.push([]);
                bfs(p, cc);
                cc++;
            }
            //console.log("mining...");
        }
    }
    return ccs;
}

function slice(image: NdArray, from_i: number = 0, to_i: number = image.shape[0], from_j: number = 0, to_j: number = image.shape[1]): NdArray {
    const si = to_i - from_i;
    const sj = to_j - from_j;
    const res = ndarray(new Array(si * sj), [si, sj]);
    for (let i = 0; i < si; i++)
        for (let j = 0; j < sj; j++)
            res.set(i, j, image.get(from_i + i, from_j + j));
    return res;
}

function crop(image: NdArray): NdArray {
    const ccs = get_ccs(image)
        //.filter(x => image.get(...x[0]) < 128)
        .sort((a, b) => b.length - a.length);
    const cc = ccs[0];
    const mini = Math.min(...cc.map(x => x[0]));
    const minj = Math.min(...cc.map(x => x[1]));
    const maxi = Math.max(...cc.map(x => x[0]));
    const maxj = Math.max(...cc.map(x => x[1]));
    return slice(image, mini, maxi + 1, minj, maxj + 1);
}

function get_verts(image: NdArray): [pos, pos, pos, pos] {
    function get_one_vert(i: (p: number, d: number) => number, j: (p: number, d: number) => number): pos {
        for (let d = 0; d < Math.max(...image.shape); d++) {
            for (let p = 0; p <= d; p++) {
                const i1 = i(p, d);
                const j1 = j(p, d);
                if (i1 < image.shape[0] && j1 < image.shape[1] && image.get(i1, j1) < 128)
                    return [i1, j1];
            }
        }
        throw new InvalidArtagImageError("Oh shit!");
    }
    return [
        get_one_vert((p, d) => p, (p, d) => d - p),
        get_one_vert((p, d) => p, (p, d) => image.shape[1] - (d - p)),
        get_one_vert((p, d) => image.shape[0] - p, (p, d) => d - p),
        get_one_vert((p, d) => image.shape[0] - p, (p, d) => image.shape[1] - (d - p)),
    ];
}

function pos_sub(a: pos, b: pos): pos {
    return [a[0] - b[0], a[1] - b[1]];
}

function pos_add_one(a: pos, b: pos): pos {
    return [a[0] + b[0], a[1] + b[1]];
}

function pos_add(...args: pos[]): pos {
    return args.reduce((x, a) => pos_add_one(x, a), [0,0]);
}

function pos_mul(a: number, b: pos): pos {
    return [a * b[0], a * b[1]];
}

function get_points(verts: [pos, pos, pos, pos]): pos[] {
    const [p0, p1, p2, p3] = verts;
    const v1 = pos_sub(p1, p0);
    const v2 = pos_sub(p3, p1);
    const v3 = pos_sub(p2, p0);
    const v4 = pos_sub(p3, p2);
    const ib = pos_mul(0.5, pos_add(v1, v4));
    const jb = pos_mul(0.5, pos_add(v3, v2));
    function get_point(i: number, j: number): pos {
        i = (i + 0.5) / ARTAG_SIZE;
        j = (j + 0.5) / ARTAG_SIZE;
        return pos_add(p0, pos_mul(i, ib), pos_mul(j, jb));
    }
    const res: pos[] = [];
    for (let j = 0; j < ARTAG_SIZE; j++) {
        for (let i = 0; i < ARTAG_SIZE; i++)
            res.push(get_point(i, j));
    }
    return res;
}

function inplace_rotate(pixels: NdArray) {
    /* https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/ */
    const size = pixels.shape[0];
    const hs = (size / 2) | 0;
    for (let p = 0; p < hs; p++) {
        for (let k = 0; k < size - p * 2 - 1; k++) {
            const p0: pos = [p + k, p];
            const p1: pos = [size - p - 1, p + k];
            const p2: pos = [size - p - 1 - k, size - p - 1];
            const p3: pos = [p, size - p - 1 - k];
            const t = pixels.get(...p0);
            pixels.set(...p0, pixels.get(...p3));
            pixels.set(...p3, pixels.get(...p2));
            pixels.set(...p2, pixels.get(...p1));
            pixels.set(...p1, t);
        }
    }
}

function rotate_artag(pixels: NdArray) {
    let lc_marker_count = 0;
    for (let i = 0; i < 4; i++) {
        if (pixels.get(ARTAG_SIZE - 2, ARTAG_SIZE - 2))
            lc_marker_count++;
        inplace_rotate(pixels);
    }
    if (lc_marker_count != 1)
        throw new InvalidArtagImageError("expected to have 1 left-bottom-corner marker, but found " + lc_marker_count.toString());
    while (!pixels.get(ARTAG_SIZE - 2, ARTAG_SIZE - 2))
        inplace_rotate(pixels);
}

function sanitize_artag(pixels: NdArray) {
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < pixels.shape[0] - 1; j++) {
            if (pixels.get(j, 0) != 0)
                throw new InvalidArtagImageError("No border around artag");
        }
        inplace_rotate(pixels);
    }
}

/**
 * converts camera output to NdArray
 */
export function numbers_to_image(image: number[]): NdArray {
    const SIZE = [120, 160];
    const result = ndarray(new Array(SIZE[0] * SIZE[1]), SIZE);
    for (let i = 0; i < SIZE[0]; i++) {
        for (let j = 0; j < SIZE[1]; j++) {
            /* (historically) use only the green channel is used */
            const val = image[j + i * SIZE[1]]
            const green = (val >> 8) & 0xff;
            result.set(i, j, green);
        }
    }
    return result;
}
/**
 * converts input from tests to NdArray
 **/
export function string_to_image(str: string): NdArray {
    const SIZE = [120, 160];
    const splitted = str.trim().split(' ');
    const result = ndarray(new Array(SIZE[0] * SIZE[1]), SIZE);
    for (let i = 0; i < SIZE[0]; i++) {
        for (let j = 0; j < SIZE[1]; j++) {
            /* we (historically) use only the green channel */
            const val = splitted[j + i * SIZE[1]].slice(2, 4);
            result.set(i, j, parseInt(val, 16));
        }
    }
    return result;
}

export function read_artag(image: NdArray): NdArray {
    filter(image);
    image = slice(image, undefined, undefined, 4, undefined);
    otsu(image);
    image = crop(image);
    const verts = get_verts(image);
    const points = get_points(verts);
    const pixels = ndarray(points.map(x => image.get(...x.map(p => p | 0)) > 128 ? 1 : 0), [ARTAG_SIZE, ARTAG_SIZE]);
    sanitize_artag(pixels);
    rotate_artag(pixels);
    return pixels;
}
