import { ARTAG_SIZE } from "./libmorat-ng";

const message_len = (ARTAG_SIZE - /*borders*/ 2) ** 2 - /*corners*/ 4;
const control_bits = Math.floor(Math.log2(message_len));
const hamming_masks: number[] = [];

// note: using little-endian for representing binaries

for (let control_bit = 0; control_bit < control_bits; ++control_bit) {
    const index = 2 ** control_bit;
    let bitmask = 0;
    for (let i = index - 1; i < message_len; i += index * 2) {
        for (let di = 0; di < index; ++di) {
            bitmask |= 1 << (i + di);
        }
    }

    hamming_masks.push(bitmask);
}

export function extract_message(hamming: number) {
    let ret = 0;
    let c = 0;
    for (let i = 2; i < message_len; ++i) {
        const l2 = Math.log2(i + 1);
        if (Math.floor(l2) !== l2) {
            ret |= ((hamming >> i) & 1) << c;
            c++;
        }
    }
    return ret;
}

function extend_message(message: number) {
    let ret = 0;
    let c = 0;
    for (let i = 0; i < message_len; ++i) {
        const l2 = Math.log2(i + 1);
        if (Math.floor(l2) !== l2) {
            const mi = message & 1;
            message >>= 1;
            ret |= mi << c;
        }
        c++;
    }
    return ret;
}

function calculate_control_bits(extended_message: number) {
    let ret = extended_message;
    for (
        let control_bit_number = 0;
        control_bit_number < control_bits;
        ++control_bit_number
    ) {
        const index = 2 ** control_bit_number - 1;
        let v = hamming_masks[control_bit_number] & extended_message;
        let bit = 0;
        while (v > 0) {
            bit ^= v & 1;
            v >>= 1;
        }
        ret |= bit << index;
    }

    return ret;
}

export function encode(message: number) {
    return calculate_control_bits(extend_message(message));
}

export function decode(hamming: number) {
    const recomputed = encode(extract_message(hamming));
    let diff = recomputed ^ hamming;
    let c = 0;
    const different_bits: number[] = [];
    while (diff > 0) {
        c++;
        const bit = diff & 1;
        diff >>= 1;
        if (bit) different_bits.push(c);
    }

    if (different_bits.length > 0) {
        console.log("Found a encode mistake, fixing...")
        const to_correct =
            different_bits.reduce((acc, curr) => acc + curr, 0) - 1;
        return extract_message(hamming ^ (1 << to_correct));
    }
    return extract_message(hamming);
}
