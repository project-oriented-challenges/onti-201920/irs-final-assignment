
import trik_brick from "trik_brick";

export abstract class SensorWrap {
    abstract update(total_elapsed_time: number, delta_time: number): void;
}

export abstract class ScalarSensor extends SensorWrap {
    abstract get_value(): number;
}

export class SmoothUltrasonicSensorWrap extends ScalarSensor {
    sensor: trik_brick.Sensor
    previous_stable_reading: number;
    previous_unstable_reading: number;
    current_reading: number;

    constructor(sensor: trik_brick.Sensor) {
        super();
        this.sensor = sensor;
        this.current_reading = sensor.read();
        this.previous_stable_reading = this.current_reading;
        this.previous_unstable_reading = this.current_reading;
    }

    update(total_elapsed_time: number, delta_time: number): void {
        const currentRead = this.sensor.read();
        if (Math.abs(currentRead - this.previous_unstable_reading) > 10)
        {
            console.log("unstable: " + this.previous_stable_reading + ", " + currentRead);
            this.previous_unstable_reading = currentRead;
            this.current_reading = this.previous_stable_reading;
        }

        this.previous_unstable_reading = currentRead;
        this.previous_stable_reading = this.previous_unstable_reading;
        
        this.current_reading = currentRead;
    }


    get_value(): number {
        return this.current_reading;
    }
}
