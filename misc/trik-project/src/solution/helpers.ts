import { Direction } from './FieldGraph';

export function normalizeAngle(angle: number) {
    var newAngle = angle % 360;
    while (newAngle <= -180) newAngle += 360;
    while (newAngle > 180) newAngle -= 360;
    return newAngle;
}

export function normalizeDirection(direction: number): Direction {
    direction = direction % 4;
    if (direction < 0) direction += 4;
    return direction;
}

export function copy<T>(x: T): T {
    return JSON.parse(JSON.stringify(x));
}

export function run_generator(generator: Iterable<unknown>): void {
    for (const _ of generator) {
    }
}
