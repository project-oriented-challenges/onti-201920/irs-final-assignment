import { WallsState } from "../FieldGraph";
import ipc_entrypoint from "./movement";
import { LLCommand, LLResult, ForwardCommand, InspectWallsResult, InspectWallsCommand, TurnCommand, ScanArtagCommand, StopCommand, ScanArtagResult } from "./low_level_interaction_proto";

// Provides a bridge between low-level control and high-level controll

export class LLInteractionClient {
    constructor(private readonly low_level_loop_coro: Generator<void, void, LLCommand>) {
        console.log("LLInteractionClient constructor");
    }

    execute_command(command: LLCommand): LLResult | undefined {
        const res = this.low_level_loop_coro.next(command);
        if (res.done)
            throw new Error("low-level control stopped, but command was issued");
        //console.log("execute result is", command.result);
        return command.result;
    }

    execute_forward(amount: number) {
        const gg = new ForwardCommand();
        gg.amount = amount;
        this.execute_command(gg);
    }

    execute_inspect_walls(): WallsState {
        const result: InspectWallsResult = <InspectWallsResult>this.execute_command(new InspectWallsCommand());
        if (result === undefined) throw new Error("Result is undefined in execute_inspect_walls()")
        return result.walls;
    }

    execute_turn(degree: number) {
        const gg = new TurnCommand();
        gg.degrees = degree;
        this.execute_command(gg);
    }

    execute_scan_artag(): ScanArtagResult {
        return <ScanArtagResult>this.execute_command(new ScanArtagCommand());
    }

    execute_stop() {
        this.execute_command(new StopCommand());
    }
}

export function create_low_level_interation_client() {
    return new LLInteractionClient(ipc_entrypoint());
}
