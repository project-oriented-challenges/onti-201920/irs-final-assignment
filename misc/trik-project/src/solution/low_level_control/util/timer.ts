
export class Timer {
    private id: number;
    
    constructor(callback: () => void, interval: number) {
        this.id = <any>setInterval(callback, interval * 1000);
    }

    clear() {
        clearInterval(<any>this.id);
    }
}
