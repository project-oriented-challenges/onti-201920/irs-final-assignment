
import trik_brick from "trik_brick";
import { Timer } from "./timer";
import { setup_gyro, packed_uint_to_sec } from "./common";

export abstract class SensorWrap {
    abstract update(total_elapsed_time: number, delta_time: number): void;
}

export abstract class ScalarSensor extends SensorWrap {
    abstract get_value(): number;
}

export abstract class VectorSensor extends SensorWrap {
    abstract get_value(): number[];
}

export class DumbScalarSensor extends ScalarSensor {
    sensor: trik_brick.Sensor;
    current_reading: number = 0;
    constructor(sensor: trik_brick.Sensor, count: number) {
        super();
        this.sensor = sensor;
    }

    get_value(): number {
        return this.current_reading;
    }

    update(total_elapsed_time: number, delta_time: number): void {
        this.current_reading = this.sensor.read();
    }

}

export class MedianScalarSensor extends SensorWrap {
    sensor: trik_brick.Sensor
    buffer: number[];
    current_reading: number = 0;

    constructor(sensor: trik_brick.Sensor, count: number) {
        super();
        this.sensor = sensor;
        this.buffer = [];
        for (let i = 0; i < count; i++)
            this.buffer.push(sensor.read());
    }

    update(total_elapsed_time: number, delta_time: number): void {
        this.buffer.push(this.sensor.read());
        this.buffer.shift();
        const sorted = this.buffer.slice();
        sorted.sort();
        this.current_reading = sorted[((sorted.length + 1) / 2) | 0];
    }


    get_value(): number {
        return this.current_reading;
    }
}

export class SmoothUltrasonicSensorWrap extends ScalarSensor {
    sensor: trik_brick.Sensor
    previous_stable_reading: number;
    previous_unstable_reading: number;
    current_reading: number;

    constructor(sensor: trik_brick.Sensor) {
        super();
        this.sensor = sensor;
        this.current_reading = sensor.read();
        this.previous_stable_reading = this.current_reading;
        this.previous_unstable_reading = this.current_reading;
    }

    update(total_elapsed_time: number, delta_time: number): void {
        const currentRead = this.sensor.read();
        if (Math.abs(currentRead - this.previous_unstable_reading) > 10)
        {
            console.log("unstable: " + this.previous_stable_reading + ", " + currentRead);
            this.previous_unstable_reading = currentRead;
            this.current_reading = this.previous_stable_reading;
        }

        this.previous_unstable_reading = currentRead;
        this.previous_stable_reading = this.previous_unstable_reading;

        this.current_reading = currentRead;
    }


    get_value(): number {
        return this.current_reading;
    }
}

export interface GyroSensor extends SensorWrap {
    get_value(): number[];
    get_time(): number;
    get_pitch(): number;
    get_roll(): number;
    get_yaw(): number;
    translate(new_zero: number): ContinuousYawGyroSensor;
    reset(): void;
}

export interface ContinuousYawGyroSensor extends GyroSensor {}

export class RawGyroSensorWrap extends VectorSensor {
    sensor: trik_brick.GyroSensor;
    value: number[];
    timer: Timer;

    constructor(sensor: trik_brick.GyroSensor) {
        super();
        this.sensor = sensor;
        this.value = [0,0,0,0];
        setup_gyro(sensor);
        //sensor.newData.connect((read, time) => this.sensor_callback(read, time));
        this.timer = new Timer(() => this.sensor_callback(), 0.050);
        //calibrate_gyro(sensor);
        //console.log(sensor.getCalibrationValues());
    }

    sensor_callback(/*values: number[], time: number*/) {
        const MDEG_TO_RAD = Math.PI / 1000 / 180;
        const raw_value = this.sensor.read();
        this.value[0] = packed_uint_to_sec(raw_value[3]);
        this.value[1] = raw_value[4] * MDEG_TO_RAD;
        this.value[2] = raw_value[5] * MDEG_TO_RAD;
        this.value[3] = -raw_value[6] * MDEG_TO_RAD;
    }

    get_value(): number[] {
        return this.value;
    }

    get_time() { return this.value[0]; }
    get_pitch() { return this.value[1]; }
    get_roll() { return this.value[2]; }
    get_yaw() { return this.value[3]; }

    /** It's updated by the timer, so there is no point in doing anything here
     */
    update(total_elapsed_time: number, delta_time: number): void {}
};

class TranslatedGyroSensorWrap implements ContinuousYawGyroSensor {
    gyro: ContinuousYawGyroSensorWrap;
    zero: number;
    constructor(gyro: ContinuousYawGyroSensorWrap, zero: number) {
        this.gyro = gyro;
        this.zero = zero;
    }

    get_value(): number[] {
        let val = this.gyro.get_value().slice();
        val[3] -= this.zero;
        return val;
    }
    get_time(): number { return this.gyro.get_time(); }
    get_pitch(): number { return this.gyro.get_pitch(); }
    get_roll(): number { return this.gyro.get_roll(); }
    get_yaw(): number { return this.gyro.get_yaw() - this.zero; }
    translate(zero: number): ContinuousYawGyroSensor { return new TranslatedGyroSensorWrap(this.gyro, this.zero + zero); }
    update(total_elapsed_time: number, delta_time: number): void {
        this.gyro.update(total_elapsed_time, delta_time);
    }

    reset() {
        this.gyro.wrapped_yaw = this.zero;
        //this.zero = this.gyro.get_value()[3];
    }
}

/**
 * trik's gyro gives angles in range [-pi;pi], but it is more convienment to have "continuous" yaw axis (other axis do not matter anyways :shrug:)
 */
export class ContinuousYawGyroSensorWrap extends RawGyroSensorWrap implements ContinuousYawGyroSensor {
    previous_yaw: number = 0;
    wrapped_yaw: number = 0;
    static circle_delta(from: number, to: number) {
        /* gives a shortest by absolute value delta on unit circle */
        if (from < 0) from += Math.PI;
        if (to < 0) to += Math.PI;
        const d1 = to - from;
        const d2 = d1 + Math.PI;
        const d3 = d1 - Math.PI;
        const a1 = Math.abs(d1), a2 = Math.abs(d2), a3 = Math.abs(d3);
        if (a1 < a2) {
            if (a1 < a3)
                return d1;
            else
                return d3;
        } else if (a2 < a3)
            return d2;
        else
            return d3;
    }

    sensor_callback(/*values: number[], time: number*/) {
        super.sensor_callback();
        let val = this.value[3];
        let pval = this.previous_yaw;
        const delta = ContinuousYawGyroSensorWrap.circle_delta(pval, val);
        this.previous_yaw = this.value[3];
        this.wrapped_yaw += delta;
        // console.log(this.wrapped_yaw);
        this.value[3] = this.wrapped_yaw;
    }

    reset() {
        this.wrapped_yaw = 0;
    }

    translate(zero: number): ContinuousYawGyroSensor { return new TranslatedGyroSensorWrap(this, zero); }
}
