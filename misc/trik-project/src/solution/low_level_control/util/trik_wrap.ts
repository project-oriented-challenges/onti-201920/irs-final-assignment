
import trik_brick from "trik_brick";
import trik_script from "trik_script";

export function take_raw_image(): number[] {
    let image = trik_brick.getStillImage();
    if (image.length == 0)
        throw new Error("Camera is not available");
    return image;
}

export function take_filtered_image(): number[] {
    let image = trik_script.getPhoto();
    if (image.length == 0)
        throw new Error("Camera is not available");
    return image;
}
