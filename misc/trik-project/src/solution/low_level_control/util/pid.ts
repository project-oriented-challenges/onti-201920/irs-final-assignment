
export class PID {
    readonly p: number;
    readonly i: number;
    readonly d: number;
    
    integral: number;
    previous_error: number;

    constructor(p: number, i: number, d: number) {
        this.p = p;
        this.i = i;
        this.d = d;
        this.integral = 0;
        this.previous_error = 0;
    }

    get_next(setpoint:number, value: number, delta_time: number): number {
        const error = setpoint - value;
        this.integral += error * delta_time;
        const derivative = (error - this.previous_error) / delta_time;
        this.previous_error = error;
        const output = this.p * error + this.i * this.integral + this.d * derivative;
        return output;
    }

    reset() {
        this.previous_error = 0;
        this.integral = 0;
    }
}

export class LimitedPID extends PID {
    min: number;
    max: number;
    
    constructor(k_p: number, t_i: number, t_d: number, min: number, max: number) {
        super(k_p, t_i, t_d);
        this.min = min;
        this.max = max;
    }

    get_next(setpoint:number, value: number, delta_time: number): number {
        let val = super.get_next(setpoint, value, delta_time);
        if (val > this.max) val = this.max;
        if (val < this.min) val = this.min;
        return val;
    }
}
