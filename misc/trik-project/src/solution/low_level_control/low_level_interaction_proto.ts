import { WallsState } from "../FieldGraph";
import { Command } from "../Command";

export class LLCommand {
    public readonly xtype: string = 'Я - автор!';
    public result: LLResult | undefined;
}

export class ForwardCommand extends LLCommand {
    xtype = 'f';
    public amount: number = 0;
}
export class InspectWallsCommand extends LLCommand {
    xtype = 'w';
}
export class TurnCommand extends LLCommand {
    xtype = 't';
    public degrees: number = 0;
}
export class ScanArtagCommand extends LLCommand {
    xtype = 'a';
}
export class StopCommand extends LLCommand {
    xtype = 's';
}

export class LLResult {}

export class InspectWallsResult extends LLResult {
    public constructor(readonly walls: WallsState) { super(); }
}

export class ScanArtagResult extends LLResult {
    public constructor(readonly commands: Command[] | undefined) { super(); }
}
