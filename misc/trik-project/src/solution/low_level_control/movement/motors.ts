

import trik_brick, { Encoder, encoder } from "trik_brick";

import { PID } from "../util/pid";
import { METERS_PER_COUNT } from "../robot_constants";

export abstract class MotorController {
    motor: trik_brick.Motor;
    encoder: trik_brick.Encoder;
    pid: PID;
    name: string;
    power_coefficient: number;
    constructor(name: string, power_coefficient: number, motor: trik_brick.Motor, encoder: trik_brick.Encoder, pid: PID) {
        this.name = name;
        this.motor = motor;
        this.encoder = encoder;
        this.pid = pid;
        this.power_coefficient = power_coefficient;

        encoder.reset();
    }

    protected validate(power: number): number {
        if (power > 100)
            power = 100;
        if (power < -100)
            power = -100;
        return power * this.power_coefficient;
    }

    protected set_motor_power(power: number) {
        this.motor.setPower(power * this.power_coefficient);
    }

    abstract update(target_speed: number, delta_time: number): void;

    brake() {
        this.motor.brake();
        this.pid.reset();
    }
}

export class SpeedyMotorController extends MotorController {
    last_encoder_position: number = 0;
    last_motor_output: number = 0;

    update(target_speed: number, delta_time: number) {
        const encoder_position = this.encoder.read();
        const encoder_velocity = (encoder_position - this.last_encoder_position) / delta_time;
        this.last_encoder_position = encoder_position;

        this.last_motor_output = this.last_motor_output + this.pid.get_next(target_speed, encoder_velocity, delta_time);
        this.last_motor_output = this.validate(this.last_motor_output);

        //console.log(0, this.name, target_speed, encoder_velocity, this.last_motor_output);

        this.set_motor_power(this.last_motor_output);
    }
}

export class PositionalMotorController extends MotorController {
    target_position: number = 0;

    update(target_speed: number, delta_time: number): void {
        const encoder_position = this.encoder.read();

        this.target_position = this.target_position + target_speed * delta_time;
        const new_power = this.pid.get_next(this.target_position, encoder_position, delta_time);

        //console.log(0, this.name, this.target_position, encoder_position, new_power);

        this.set_motor_power(this.validate(new_power));
    }
    
    brake() {
        super.brake();
        this.target_position = 0;
        this.encoder.reset();
    }
}

export class Encoders {
    left: Encoder;
    right: Encoder;
    constructor(left: Encoder, right: Encoder) {
        this.left = left;
        this.right = right;
    }
    get_position(): [number, number] {
        return [this.left.read(), this.right.read() ];
    }
    get_distance_from(position: [number, number]): number {
        const [pl, pr] = position;
        const [l, r] = this.get_position();
        const [dl, dr] = [METERS_PER_COUNT * (l - pl), METERS_PER_COUNT * (r - pr)];
        //console.log(pl, pr, l, r, dl, dr);
        return Math.abs(dl + dr) / 2;
    }
}

export function make_motor(name: string, port_number: string, power_coefficient: number, p: number, i: number, d: number): MotorController {
    const pid = new PID(p, i, d);
    const encoder = trik_brick.encoder("E" + port_number);
    const motor = trik_brick.motor("M" + port_number);
    return new PositionalMotorController(name, power_coefficient, motor, encoder, pid);
}

export function make_encoders(left_port: string, right_port: string) {
    const left = trik_brick.encoder("E" + left_port);
    const right = trik_brick.encoder("E" + right_port);
    return new Encoders(left, right);
}
