
import { SmoothUltrasonicSensorWrap, ContinuousYawGyroSensorWrap, SensorWrap, ScalarSensor, ContinuousYawGyroSensor } from "../util/sensor_wrap";
import { make_motor, MotorController } from "./motors";
import { WHEEL_DISTANCE } from "../robot_constants";
import { PID } from "../util/pid";



// rotate k: 2.555

function cap_speed(speed: number) {
    return Math.sign(speed) * Math.min(500, Math.abs(speed));
}

export class Motors {
    readonly motor_left: MotorController;
    readonly motor_right: MotorController;
    constructor(motor_left: MotorController, motor_right: MotorController) {
        this.motor_left = motor_left;
        this.motor_right = motor_right;
    }
    update(left_speed: number, right_speed: number, delta_time: number) {
        this.motor_left.update(left_speed, delta_time);
        this.motor_right.update(right_speed, delta_time);
    }

    brake() {
        this.motor_left.brake();
        this.motor_right.brake();
    }
}

export class RulingMovementController  {
    readonly motors: Motors;
    static readonly D = WHEEL_DISTANCE;

    readonly speedup: number;
    constructor(motors: Motors, speedup: number) {
        this.motors = motors;
        this.speedup = speedup;
    }
    /**
     * Positive radius means turning right, negative - left, +-Infinity - forward
     */

    update(target_speed: number, rule_radius: number, total_elapsed_time: number, delta_time: number): void {
        target_speed = cap_speed(target_speed);
        let actual_target_speed = Math.sign(target_speed) * Math.min(Math.abs(target_speed), total_elapsed_time * this.speedup);
        if (rule_radius == Infinity || rule_radius == -Infinity)
        {
            this.motors.update(actual_target_speed, actual_target_speed, delta_time);
        }
        else
        {
            const R_m = Math.abs(rule_radius);
            const R_D = R_m + RulingMovementController.D / 2;
            const R = R_m - RulingMovementController.D / 2;
            const speed1 = cap_speed(actual_target_speed * R / R_m);
            const speed2 = cap_speed(actual_target_speed * R_D / R_m);

            //console.log(total_elapsed_time, actual_target_speed, speed1, speed2);

            // speed2 >= speed1

            if (rule_radius > 0)
                this.motors.update(speed2, speed1, delta_time);
            else
                this.motors.update(speed1, speed2, delta_time);
        }
    }

    brake() {
        this.motors.brake();
    }
}

export abstract class DirectionController {
    /** Returns desired curvature or null if don't have information
     */
    abstract update(target_speed: number, total_elapsed_time: number, delta_time: number): number | null;
    reset() {}
}

export class GyroDirectionController extends DirectionController {
    readonly gyro: ContinuousYawGyroSensor;
    static readonly coefficient: number = 20;

    constructor(gyro: ContinuousYawGyroSensor) {
        super();
        this.gyro = gyro;
    }
    update(target_speed: number, total_elapsed_time: number, delta_time: number): number | null {
        const error = this.gyro.get_yaw();
        const curvature = Math.sign(target_speed) * error * GyroDirectionController.coefficient;
        return curvature;
    }

    reset_gyro() {
        this.gyro.reset();
    }
}

export class WallProximityDirectionController extends DirectionController {
    readonly proximity: ScalarSensor;
    static readonly COEFFICIENT: number = 1;
    readonly coefficient: number;
    readonly target_proximity: number;
    readonly regulator: PID;
    error: number = 0;
    active: boolean = false;

    constructor(proximity: ScalarSensor, target_proximity: number, is_right: boolean) {
        super();
        this.proximity = proximity;
        this.regulator = new PID(0.4, 0, 0.1);
        if (!is_right)
            this.coefficient = -WallProximityDirectionController.COEFFICIENT;
        else
            this.coefficient = WallProximityDirectionController.COEFFICIENT;
        this.target_proximity = target_proximity;
    }

    update(target_speed: number, total_elapsed_time: number, delta_time: number): number | null {
        const prox_value = this.proximity.get_value();
        this.error = this.target_proximity - prox_value;
        //console.log(prox_value, error);
        this.active = prox_value < 25;
        if (!this.active)
            return null;
        const regulated_value = this.regulator.get_next(0, this.error, delta_time);
        return this.coefficient * regulated_value;
    }

    reset() {
        this.regulator.reset();
    }
}

export class CompositeDirectionController extends DirectionController {
    elements: [DirectionController, number][];

    constructor(elements: [DirectionController, number][]) {
        super();
        this.elements = elements;
    }

    update(target_speed: number, total_elapsed_time: number, delta_time: number): number | null {
        const results = this.elements
            .map((v) => [v[0].update(target_speed, total_elapsed_time, delta_time), v[1]])
            .filter((p): p is [number, number] => p[0] !== null);
        if (results.length == 0)
            return null;
        return results.reduce((a, v) => a + v[0] * v[1], 0) / results.reduce((a, v) => a + v[1], 0);
    }

}

export class ForwardMovementController {
    readonly direction: CompositeDirectionController;
    readonly ruler: RulingMovementController;
    static readonly coefficient: number = 20;
    private previous_errors: number[] = [];
    private readonly wall_controllers: WallProximityDirectionController[];
    private readonly gyro_controller: GyroDirectionController;

    constructor(ruler: RulingMovementController, direction: CompositeDirectionController) {
        this.direction = direction;
        this.ruler = ruler;
        this.wall_controllers = <WallProximityDirectionController[]>direction.elements.map(x => x[0]).filter(x => x instanceof WallProximityDirectionController);
        this.gyro_controller = <GyroDirectionController>direction.elements.map(x => x[0]).filter(x => x instanceof GyroDirectionController)[0];
    }

    private reset_gyro() {
        this.gyro_controller.reset_gyro();
        console.log("gyro reset");
    }

    private update_gyro_reset(total_elapsed_time: number) {
        const current_errors = this.wall_controllers.filter(x => x.active).map(x => x.error);
        const ok = Math.max(...this.previous_errors.map(x => Math.abs(x)).slice(undefined, -1)) < 3 && this.previous_errors.length > 20;
        if (this.previous_errors.length >= 60 && ok) {
            console.log("we are stable for a pretty long time. resetting gyro...; previous_errors =", JSON.stringify(this.previous_errors))
            this.previous_errors = [];
            this.reset_gyro();
            return;
        }
        if (current_errors.length == 0) {
            if (this.previous_errors.length != 0) {
                console.log("we have lost walls! Were we moving ok?", (ok ? "YES;" : "no...;"), "previous_errors =", JSON.stringify(this.previous_errors));
                this.previous_errors = [];
                if (ok)
                    this.reset_gyro();
            }
            return;
        }
        this.previous_errors.push(Math.max(...current_errors));
        //return false;
    }

    update(target_speed: number, total_elapsed_time: number, delta_time: number): void {
        const curvature = this.direction.update(target_speed, total_elapsed_time, delta_time) ?? 0;

        this.update_gyro_reset(total_elapsed_time);
        //console.log(target_speed, curvature, 1 / curvature);

        this.ruler.update(target_speed, 1 / curvature, total_elapsed_time, delta_time);
    }
    brake(): void {
        this.ruler.brake();
    }
}

export class RotationMovementController {
    constructor(readonly motors: Motors, readonly speedup: number) {
        this.motors = motors;
        this.speedup = speedup;
    }

    update(target_speed: number, total_elapsed_time: number, delta_time: number): void {
        target_speed = cap_speed(target_speed);
        let actual_target_speed = Math.sign(target_speed) * Math.min(Math.abs(target_speed), total_elapsed_time * this.speedup);
        this.motors.update(actual_target_speed, -actual_target_speed, delta_time);
    }
    brake() {
        this.motors.brake();
    }
}
