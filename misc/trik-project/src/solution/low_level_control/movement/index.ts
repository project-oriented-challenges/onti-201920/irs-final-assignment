
import trik_brick, { display } from "trik_brick";

import { get_time, delay } from "../util/common";
import { ScalarSensor, ContinuousYawGyroSensorWrap, MedianScalarSensor, ContinuousYawGyroSensor } from "../util/sensor_wrap";
import { make_motor, Encoders, make_encoders } from "./motors";
import { ForwardMovementController, GyroDirectionController, RulingMovementController, WallProximityDirectionController, CompositeDirectionController, Motors, RotationMovementController } from "./movement";
import { LEFT_MOTOR_COEFFICIENT, RIGHT_MOTOR_COEFFICIENT, TRIK_90, MOVE_SPEED, WHEEL_TO_CENTER_DISTANCE, ROTATE_SPEED, TRIK_180, CELL_FRONT_DISTANCE, MOTOR_PID_COEEFICIENTS, MOVE_SPEEDUP as MOVE_SPEEDUP, ROTATE_SPEEDUP } from "../robot_constants";
import { InspectWallsResult, ScanArtagCommand, LLCommand, ForwardCommand, LLResult, InspectWallsCommand, TurnCommand, StopCommand, ScanArtagResult } from "../low_level_interaction_proto";
import { take_filtered_image } from "../util/trik_wrap";
import { numbers_to_image, read_artag, InvalidArtagImageError } from "../../artag/libmorat-ng";
import { artag_decode } from "../../artag/artag_decode";

const MOTOR_LEFT_PORT = "2"
const MOTOR_RIGHT_PORT = "1"

const FRONT_SENSOR_PORT = "D1"
const BACK_SENSOR_PORT = "D2"
const LEFT_SENSOR_PORT = "A3"
const RIGHT_SENSOR_PORT = "A2"

abstract class MovementOperation {
    constructor(name: string) {
        this.name = name;
    }

    abstract update(total_elapsed_time: number, delta_time: number): boolean;
    readonly name: string;
}

class MoveToProximityOperation extends MovementOperation {
    readonly proximity_sensor: ScalarSensor;
    readonly target_distance: number;
    readonly movement_controller: ForwardMovementController;
    readonly target_speed: number;
    stop_counter = 0;


    constructor(target_speed: number, proximity_sensor: ScalarSensor, movement_controller: ForwardMovementController, target_distance: number, name: string) {
        super(name);

        this.proximity_sensor = proximity_sensor;
        this.movement_controller = movement_controller;
        this.target_distance = target_distance;
        this.target_speed = target_speed;
    }

    update(total_elapsed_time: number, delta_time: number): boolean {
        const current_distance = this.proximity_sensor.get_value()
        if (current_distance == -1)
            throw new Error("current_distance is -1");

        const error = Math.max(current_distance - this.target_distance, 0);

        //console.log(current_distance);

        //, error, regulated_target_speed, this.stop_counter);

        if (error < 2 || this.stop_counter > 0)
        {
            this.stop_counter += 1;
            this.movement_controller.update(0, total_elapsed_time, delta_time);
            if (this.stop_counter > 0)
            {
                this.movement_controller.brake();
                return false
            };
        }
        else
        {
            const regulated_target_speed = Math.sign(this.target_speed) * Math.min(Math.abs(this.target_speed), error * 10);
            this.movement_controller.update(regulated_target_speed, total_elapsed_time, delta_time);
        }
        return true;
    }
}

class RotateOperation extends MovementOperation {
    constructor(readonly target_speed: number, readonly controller: RotationMovementController, readonly gyro: ContinuousYawGyroSensor, name: string) {
        super(name);
    }

    update(total_elapsed_time: number, delta_time: number): boolean {
        const SLOWDOWN_BEGIN = Math.PI / 4;
        const error = this.gyro.get_yaw();
        const capped_error = Math.sign(error) * Math.min(Math.abs(error), SLOWDOWN_BEGIN);
        //console.log(error, capped_error);
        if (Math.abs(error) < Math.PI / 180) {
            this.controller.brake();
            return false;
        }
        this.controller.update(capped_error * this.target_speed / SLOWDOWN_BEGIN, total_elapsed_time, delta_time);
        return true;
    }
}


class MoveMetersOperation extends MovementOperation {
    readonly movement_controller: ForwardMovementController;
    readonly target_speed: number;
    readonly initial_position: [number, number];
    readonly target_distance: number;
    readonly encoders: Encoders;

    constructor(target_speed: number, movement_controller: ForwardMovementController, encoders: Encoders, distance: number, name: string) {
        super(name);
        this.movement_controller = movement_controller;
        this.target_speed = target_speed;
        this.initial_position = encoders.get_position();
        this.target_distance = distance;
        this.encoders = encoders;
    }

    update(total_elapsed_time: number, delta_time: number): boolean {
        const position = this.encoders.get_distance_from(this.initial_position);
        const error = this.target_distance - position;
        if (error < 0.005) {
            this.movement_controller.update(0, total_elapsed_time, delta_time);
            this.movement_controller.brake();
            return false;
        }

        const regulated_target_speed = Math.sign(this.target_speed) * Math.min(Math.abs(this.target_speed), error * 10000);

        //console.log(error, regulated_target_speed);
        this.movement_controller.update(regulated_target_speed, total_elapsed_time, delta_time);

        return true;
    }
}

class DelayOperation extends MovementOperation {
    constructor(readonly delay: number, name: string) {
        super(name);
    }

    update(total_elapsed_time: number, delta_time: number): boolean {
        return total_elapsed_time < this.delay;
    }
}


let [p, i, d] = MOTOR_PID_COEEFICIENTS;
const motor_left = make_motor("L", MOTOR_LEFT_PORT, LEFT_MOTOR_COEFFICIENT, p, i, d); // new SpeedyMotorWrapper("L", motor1, encoder1, pid1);
const motor_right = make_motor("R", MOTOR_RIGHT_PORT, RIGHT_MOTOR_COEFFICIENT, p, i, d); // new SpeedyMotorWrapper("R", motor2, encoder2, pid2);
const motors = new Motors(motor_left, motor_right);

const encoders = make_encoders(MOTOR_LEFT_PORT, MOTOR_RIGHT_PORT);

const front_sensor = new MedianScalarSensor(trik_brick.sensor(FRONT_SENSOR_PORT), 3);
const back_sensor = new MedianScalarSensor(trik_brick.sensor(BACK_SENSOR_PORT), 3);
const left_sensor = new MedianScalarSensor(trik_brick.sensor(LEFT_SENSOR_PORT), 3);
const right_sensor = new MedianScalarSensor(trik_brick.sensor(RIGHT_SENSOR_PORT), 3);


let gyro: ContinuousYawGyroSensor = new ContinuousYawGyroSensorWrap(trik_brick.gyroscope());
function make_forward_movement_controller() {
    const ruler = new RulingMovementController(motors, MOVE_SPEEDUP);
    const direction_rwall = new WallProximityDirectionController(right_sensor, 13.5, true);
    const direction_lwall = new WallProximityDirectionController(left_sensor, 13.5, false);
    const direction_gyro = new GyroDirectionController(gyro);
    const composite_ruler = new CompositeDirectionController([[direction_gyro, 0.2], [direction_rwall, 0.4], [direction_lwall, 0.4]]);
    const current_controller = new ForwardMovementController(ruler, composite_ruler);
    return current_controller;
}

function* make_move_until(speed: number, proximity: number, name: string) {
    yield new MoveToProximityOperation(speed, speed < 0 ? back_sensor : front_sensor, make_forward_movement_controller(), proximity, name);
}

function* make_move_to(speed: number, distance: number, name: string) {
    yield new MoveMetersOperation(speed, make_forward_movement_controller(), encoders, distance, name);
}

function* make_raw_rotation(speed: number, rotation: number, name: string) {
    yield new RotateOperation(speed, new RotationMovementController(motors, ROTATE_SPEEDUP), gyro.translate(-rotation), name);
}

function* make_noop(name: string) {
    yield new DelayOperation(1 / 60 + 0.01, name);
}

function* definite_proximity(sensor: ScalarSensor[]): Generator<MovementOperation, [number, number][]> {
    /* TODO: maybe add some filtering */
    const vv = [];
    for (let i = 0; i < 5; i++) {
        vv.push(sensor.map(x => x.get_value()));
        yield* make_noop("definite_proximity_yield");
    }

    console.log("vv is", JSON.stringify(vv));
    let res: [number, number][] = new Array(vv[0].length);
    for (let i = 0; i < res.length; i++)
        res[i] = [0, 0];
    for (let i = 0; i < vv.length; i++)
        for (let j = 0; j < vv[i].length; j++)
            res[j][0] += vv[i][j];

    res = res.map(x => [x[0] / vv.length, x[1]]);

    for (let i = 0; i < vv.length; i++)
        for (let j = 0; j < vv[i].length; j++)
            res[j][1] += Math.pow(res[j][0] - vv[i][j], 2);
    res = res.map(x => [x[0], x[1] / vv.length]);

    console.log("res is", JSON.stringify(res));
    return res;
}

function* make_cell_rotation(rotation: number, name: string) {
    rotation = rotation | 0;
    let degree = 0;
    switch (rotation) {
        case 0:
            degree = 0;
            break;
        case 1:
            degree = TRIK_90;
            break;
        case 2:
            degree = TRIK_180;
            break;
        case 3:
            degree = -TRIK_90;
            break;
    }
    yield* make_move_to(ROTATE_SPEED, WHEEL_TO_CENTER_DISTANCE, "move_forward_before_rotation");
    yield* make_raw_rotation(ROTATE_SPEED, degree, "rotate");
    gyro = gyro.translate(-degree);
    yield* make_move_to(-ROTATE_SPEED, WHEEL_TO_CENTER_DISTANCE, "move_backwards_after_rotation");
}

function* make_onwards_one(): Iterable<MovementOperation> {
    const f = (yield* definite_proximity([front_sensor]))[0][0];
    console.log("Forward distance:", f);
    if (f < 20 && f != -1) throw Error("Won't move into wall");
    else if (f - 40 < 15) yield* make_move_until(MOVE_SPEED, CELL_FRONT_DISTANCE, "move_forward_until_wall");
    else yield* make_move_to(MOVE_SPEED, 0.4, "move_forward_tacho");
}

function inspect_wall(distance: number, error: number, is_infrared: boolean): number | undefined {
    if (is_infrared) {
        if (distance < 35)
            return 0;
        else
            return 1;
    } else {
        if (error > 5)
            return undefined;
        if (distance == -1)
            throw new Error("distance is -1");
        if (distance < 30)
            return 0;
        //if (distance < 70)
            return 1;
        //if (distance < 100)
        //    return 2;
        //return 3;
    }
}

function* make_onwards_many(count: number): Iterable<MovementOperation> {
    if (count == 1)
        yield* make_onwards_one();
    else {
        const f = (yield* definite_proximity([front_sensor]))[0];
        console.log("Forward distance:", f);
        const free = inspect_wall(f[0], f[1], false) ?? 0;
        if (f[0] < 20 && f[0] != -1) throw Error("Won't move into wall");
        if (free == count && free < 2)
            yield* make_move_until(MOVE_SPEED, CELL_FRONT_DISTANCE, "move_forward_until_wall");
        else
            yield* make_move_to(MOVE_SPEED, 0.4 * count, "move_forward_tacho");
    }
}

function* inspect_walls(): Generator<MovementOperation, InspectWallsResult> {
    //const aaa: [number, boolean][] = [];
    const sens = [front_sensor, back_sensor, left_sensor, right_sensor];
    const bbb = yield* definite_proximity(sens);
    const ccc: [number, number, boolean][] = new Array(bbb.length);
    for (let i = 0; i < sens.length; i++) {
        ccc[i] = [bbb[i][0], bbb[i][1], sens[i] == left_sensor || sens[i] == right_sensor];
    }

    console.log("inspect_walls:", bbb);
    let [f, b, l, r] = ccc.map(x => inspect_wall(x[0], x[1], x[2]));
    if (f == undefined || b == undefined)
        return yield* inspect_walls();
    return new InspectWallsResult({
        f_free: f,
        b_free: b,
        l_free: l ?? 1,
        r_free: r ?? 1,
        // f_wall: f < 2 ? f : undefined,
        // b_wall: b < 2 ? b : undefined,
        // l_wall: (l ?? 1) < 1 ? l : undefined,
        // r_wall: (r ?? 1) < 1 ? r : undefined,
    });
}

function scan_artag(): ScanArtagResult {
    console.log("scanning artag...");
    const raw_image = take_filtered_image();
    display().show(raw_image, 160, 120, "rgb32");
    const image = numbers_to_image(raw_image);
    try {
        const data = read_artag(image);
        const commands = artag_decode(data);
        return new ScanArtagResult(commands);
    } catch (e) {
        if (e instanceof InvalidArtagImageError)
            return new ScanArtagResult(undefined);
        throw e;
    }
}




function* ipc_logic(): Generator<MovementOperation | undefined, void, LLCommand | undefined> {
    console.log("ipc_logic");
    while (true) {
        const command = yield;

        console.log("got command (2):", command);

        if (command instanceof ForwardCommand) {
            for (const p of make_onwards_many(command.amount))
                yield p;
            command.result = new LLResult();
        } else if (command instanceof InspectWallsCommand) {
            yield* make_noop("yield_for_sensors");
            command.result = yield* inspect_walls();
        } else if (command instanceof TurnCommand) {
            const magic = (((-command.degrees / 90) | 0) + 4) % 4;
            yield* make_cell_rotation(magic, "rotate_" + magic.toString());
            command.result = new LLResult();
        } else if (command instanceof ScanArtagCommand) {
            yield* make_noop("yield_for_sensors");
            command.result = scan_artag();
        } else if (command instanceof StopCommand) {
            command.result = new StopCommand();
            break;
        } else {
            throw new Error("Got unknown command");
        }
    }
}

/* some coroutine spaghetti is going on. But it is better than having two threads */
function* execute_logic(ipc_logic_generator: Generator<MovementOperation | undefined, void, LLCommand | undefined>): Generator<void, void, LLCommand>
{
    const logic = ipc_logic_generator[Symbol.iterator]();
    let current_operation: MovementOperation | null = null;
    let current_command: LLCommand = yield;
    logic.next(current_command);
    //console.log("got initial command", current_command)

    let prev_time = get_time();
    let start_time = prev_time;
    delay(0.01);

    while (true) {
        const time = get_time();
        const delta_time = time - prev_time;
        const total_elapsed_time = time - start_time;

        prev_time = time;

        front_sensor.update(total_elapsed_time, delta_time);
        back_sensor.update(total_elapsed_time, delta_time);
        left_sensor.update(total_elapsed_time, delta_time);
        right_sensor.update(total_elapsed_time, delta_time);
        gyro.update(total_elapsed_time, delta_time);

        if (current_operation == null) {
            const value = logic.next(current_command);
            //console.log("got operation", value.value)
            if (value.done)
                break;
            if (value.value == undefined) {
                //console.log("yielding to high level...")
                current_command = yield;
                //console.log("got:", current_command)
                continue;
            }
            start_time = prev_time = get_time();
            current_operation = value.value!;
            delay(0.01);
            console.log("new operation started:", start_time, current_operation.name);
            continue;
        }
        if (!current_operation.update(total_elapsed_time, delta_time)) {
            console.log("Operation '" + current_operation.name + "' has Completed");
            current_operation = null;
        }

        delay(1 / 60);
    }
    console.log("Control loop exited");
}

/* Just in case */
delay(1);

//execute_logic(test_logic());
//execute_logic(left_handle_rule_logic());

function ipc_entrypoint(): Generator<void, void, LLCommand> {
    console.log("worker entry");
    const co = execute_logic(ipc_logic());
    co.next();
    return co;
}

export default ipc_entrypoint;
