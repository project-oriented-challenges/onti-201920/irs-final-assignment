import trik_mailbox from 'trik_mailbox';

/* specific to construction */
export const WHEEL_DISTANCE = 0.175;
export const WHEEL_RADIUS = 0.03969885586091524; /* computed by moving trik and measuring encoders' values */ //0.0425
export const COUNTS_PER_REVOLUTION = 360;
export const COUNTS_PER_METER =
    COUNTS_PER_REVOLUTION / (2 * WHEEL_RADIUS * Math.PI);
export const METERS_PER_COUNT = 1 / COUNTS_PER_METER;
export const WHEEL_TO_CENTER_DISTANCE = 0.04;

export const CELL_FRONT_DISTANCE = 7.5;
export const MOVE_SPEED = 300;
export const ROTATE_SPEED = 200;
export const MOVE_SPEEDUP = 300;
export const ROTATE_SPEEDUP = 100;

/* specific to robot instance */
export const RIGHT_MOTOR_COEFFICIENT = 1;
export const LEFT_MOTOR_COEFFICIENT = 1;

/* some PID coefficients */
// 0.2   0.2 0     for non-accumulating speedy regulator
// 0.05  0   0.001 for accumulating speedy regulator
// 0.8   0.1 0     for positional regulator
// validated at 11.1824 V
export const MOTOR_PID_COEEFICIENTS = [0.8, 0.1, 0.005];

/* Reduces overshoot when rotating using gyro */
export let TRIK_90: number;
export let TRIK_180: number;

if (trik_mailbox.myHullNumber() == 0) {
    TRIK_90 =
        //0.475
        //0.5
        0.48 * Math.PI;
    TRIK_180 = 0.95 * Math.PI;
} else {
    TRIK_90 =
        //0.475
        0.485
        // 0.48
        * Math.PI;
    TRIK_180 =
        // 0.95
        1
        * Math.PI;
}
