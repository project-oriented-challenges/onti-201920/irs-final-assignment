import '../../../shim-global';

import script from 'trik_script';
import brick from 'trik_brick';

import { RobotSimulator } from '../../robots/RobotSimulator';
import { Direction } from '../../FieldGraph';
import { run_generator } from '../../helpers';

const INPUT_FILE = 'task1_00.txt';

const lines = script.readAll(INPUT_FILE);
const [dnum] = lines[0]
    .trim()
    .split(' ')
    .map((x) => parseInt(x, 10));
const [xf, yf] = lines[1]
    .trim()
    .split(' ')
    .map((x) => parseInt(x, 10));

const dirMap: { [num: number]: Direction } = {
    0: Direction.Up,
    1: Direction.Right,
    2: Direction.Down,
    3: Direction.Left,
};

const robot = new RobotSimulator(dirMap[dnum], true);
const initialNode = robot.localNode;
robot.scan();
robot.localize(true);
run_generator(robot.moveToLocalMapNode(initialNode));

const [yfree, xfree] = robot.getReachableCellNearOccupiedCell([
    yf + robot.dy!,
    xf + robot.dx!,
])!;

brick.display().clear();
brick.display().addLabel(`(${xfree - robot.dx!},${yfree - robot.dy!})`, 1, 1);
brick.display().redraw();
