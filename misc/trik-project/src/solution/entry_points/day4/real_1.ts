import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import mailbox from 'trik_mailbox';
import { Direction, negateDirection, Node } from '../../FieldGraph';
import { RobotReal } from '../../robots/RobotReal';
import { delay } from '../../low_level_control/util/common';
import {
    run_generator,
    normalizeAngle,
    normalizeDirection,
} from '../../helpers';
import { Command } from '../../Command';

const FIRST_ROBOT_HULL_NUM = 0;
const FIRST_ROBOT_DIR = Direction.Up;

const SECOND_ROBOT_HULL_NUM = 1;
const SECOND_ROBOT_DIR = Direction.Left;

switch (mailbox.myHullNumber()) {
    case FIRST_ROBOT_HULL_NUM: {
        const robot = new RobotReal(FIRST_ROBOT_DIR);
        const localMapStart = robot.localNode;

        mailbox.receive(true); // wait for the second robot finish scanning artag & moving

        robot.scan();
        let relative_dir: Direction;
        let scanned_node: Node;

        for (const cell of robot.walk()) {
            run_generator(robot.moveToLocalMapNode(cell));

            console.log('sending stuff to', SECOND_ROBOT_HULL_NUM);
            mailbox.send(
                SECOND_ROBOT_HULL_NUM,
                JSON.stringify({ type: 'rescan' })
            );
            const msg = JSON.parse(mailbox.receive(true));
            if (msg !== null) {
                console.log('WHOOOOP!', JSON.stringify(msg));
                const { dwall }: { dwall: Direction } = msg;
                relative_dir = negateDirection(dwall);
                scanned_node = robot.localNode;

                break; // from now on we should use optimal localization algorithm
            }
        }

        robot.localize();

        console.log('localized');

        let localCell: Node; // second robot coords in the first robot local coordinate system
        switch (relative_dir!) {
            case Direction.Right:
                localCell = [scanned_node![0], scanned_node![1] + 1];
                break;
            case Direction.Up:
                localCell = [scanned_node![0] - 1, scanned_node![1]];
                break;
            case Direction.Left:
                localCell = [scanned_node![0], scanned_node![1] - 1];
                break;
            case Direction.Down:
                localCell = [scanned_node![0] + 1, scanned_node![1]];
                break;
        }

        run_generator(robot.moveNearOccupiedCell(localCell!));

        brick.display().clear();
        brick.display().addLabel(`(${robot.xReal},${robot.yReal})`, 1, 1);
        brick.display().redraw();
        brick.playTone(1000, 100);

        delay(10);

        mailbox.send(
            SECOND_ROBOT_HULL_NUM,
            JSON.stringify({
                type: 'coords',
                args: { node: localCell!, dx: robot.dx, dy: robot.dy },
            })
        );

        const secondStart: Node = JSON.parse(mailbox.receive(true));
        const secondStartLocal: Node = [
            secondStart[0] + robot.dy!,
            secondStart[1] + robot.dx!,
        ];

        console.log(
            'Starts:',
            JSON.stringify([secondStartLocal, localMapStart])
        );

        const dfirst =
            Math.abs(robot.yLocal - localMapStart[0]) +
            Math.abs(robot.xLocal - localMapStart[1]);
        const dsecond =
            Math.abs(robot.yLocal - secondStartLocal[0]) +
            Math.abs(robot.xLocal - secondStartLocal[1]);

        if (dfirst <= dsecond) {
            try {
                run_generator(robot.moveToLocalMapNode(localMapStart));
            } catch (e) {
                if (e instanceof RangeError) {
                    run_generator(robot.moveToLocalMapNode(secondStartLocal));
                } else throw e;
            }
        } else {
            try {
                run_generator(robot.moveToLocalMapNode(secondStart));
            } catch (e) {
                if (e instanceof RangeError) {
                    run_generator(robot.moveToLocalMapNode(localMapStart));
                } else throw e;
            }
        }

        mailbox.send(
            SECOND_ROBOT_HULL_NUM,
            JSON.stringify({
                type: 'stop',
                args: null,
            })
        );

        brick.display().clear();
        brick.display().addLabel('finish', 1, 20);
        brick.display().redraw();
        delay(10);
        break;
    }
    case SECOND_ROBOT_HULL_NUM: {
        /* oh well */
        mailbox.connect('192.168.77.1');

        const robot = new RobotReal(SECOND_ROBOT_DIR, false);

        const secondStart = robot.localNode;

        const dir_initial = robot.direction;
        const commands = robot.findArtag();
        if (commands === undefined) throw new Error('Cannot read artag!!1');
        const dir_delta = normalizeDirection(dir_initial - robot.direction);
        const rot_cmds = Array(dir_delta).fill(Command.TurnLeft);
        run_generator(robot.runCommands(rot_cmds));

        // run_generator(robot.runCommandsRaw(commands));
        run_generator(robot.runCommands(commands));

        mailbox.send(FIRST_ROBOT_HULL_NUM, 'null');

        brick.playTone(1000, 100);

        brick.display().clear();
        brick.display().addLabel('finish', 1, 20);
        brick.display().redraw();

        const walls_initial = robot.get_walls_neighbors();
        let wait = true;

        while (wait) {
            const messageJson = mailbox.receive(true);
            const message: { type: string; args: any } = JSON.parse(
                messageJson
            );
            switch (message.type) {
                case 'rescan': {
                    const walls = robot.get_walls_neighbors();
                    // only one wall can change
                    const newWalls = walls.filter(
                        (x) => !walls_initial.includes(x)
                    );
                    const removedWalls = walls_initial.filter(
                        (x) => !walls.includes(x)
                    );
                    let dwall: Direction | undefined = undefined;
                    if (newWalls.length) {
                        dwall = newWalls[0];
                    } else if (removedWalls.length) {
                        dwall = newWalls[0];
                    }

                    if (dwall !== undefined) {
                        mailbox.send(
                            FIRST_ROBOT_HULL_NUM,
                            JSON.stringify({
                                dwall,
                            })
                        );
                    } else mailbox.send(FIRST_ROBOT_HULL_NUM, 'null');
                    break;
                }

                case 'coords': {
                    const {
                        args: { node, dx, dy },
                    }: {
                        args: { node: Node; dx: number; dy: number };
                    } = message;

                    // local_first = real + d
                    // real = local_first - d
                    // d_second = local_second - real

                    const yReal = node[0] - dy;
                    const xReal = node[1] - dx;

                    robot.dx = robot.localNode[1] - yReal;
                    robot.dy = robot.localNode[0] - xReal;

                    console.log(
                        'sending to first: ',
                        JSON.stringify([
                            secondStart[0] - robot.dy,
                            secondStart[1] - robot.dx,
                        ])
                    );
                    mailbox.send(
                        FIRST_ROBOT_HULL_NUM,
                        JSON.stringify([
                            secondStart[0] - robot.dy,
                            secondStart[1] - robot.dx,
                        ])
                    );

                    break;
                }

                case 'stop': {
                    wait = false;
                    break;
                }
            }
        }
        break;
    }
    default: {
        throw new Error(`Unknown hull number: ${mailbox.myHullNumber()}`);
    }
}
