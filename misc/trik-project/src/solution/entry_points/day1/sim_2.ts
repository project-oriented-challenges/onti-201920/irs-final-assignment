import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { RobotSimulator } from '../../robots/RobotSimulator';
import { Direction } from '../../FieldGraph';
import { run_generator } from '../../helpers';

const INPUT_FILE = 'task2_04.txt';
const dirMap: { [num: number]: Direction } = {
    0: Direction.Up,
    1: Direction.Right,
    2: Direction.Down,
    3: Direction.Left,
};

const lines = script.readAll(INPUT_FILE);
const [x0, y0, dnum] = lines[0].trim().split(' ').map((x) => parseInt(x, 10));
const [xf, yf] = lines[1].trim().split(' ').map((x) => parseInt(x, 10));

const robot = new RobotSimulator(dirMap[dnum], true);
robot.scan();

const dx = xf - x0;
const dy = yf - y0;
console.log(dx, dy);
console.log(robot.localNode);

run_generator(robot.moveToLocalMapNode([robot.yLocal + dy, robot.xLocal + dx]));

brick.display().clear();
brick.display().addLabel('finish', 1, 1);
brick.display().redraw()
