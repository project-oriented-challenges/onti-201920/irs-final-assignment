import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { Command } from '../../Command';
import { Direction } from '../../FieldGraph';
import { RobotReal } from '../../robots/RobotReal';
import { delay } from '../../low_level_control/util/common';
import { run_generator } from '../../helpers';

const robot = new RobotReal(Direction.Left); // any direction, but we must specify it

// Test 2
const commands: Command[] = [1, 3, 3, 0, 0, 0, 2, 2, 3, 0, 3]

run_generator(robot.runCommandsRaw(commands));

brick.display().clear();
brick.display().addLabel('finish', 1, 1);
brick.display().redraw();

delay(15);
