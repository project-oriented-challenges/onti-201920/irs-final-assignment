import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { Direction } from '../../FieldGraph';
import { RobotReal } from '../../robots/RobotReal';
import { delay } from '../../low_level_control/util/common';
import { run_generator } from '../../helpers';

const dirMap: { [num: number]: Direction } = {
    0: Direction.Up,
    1: Direction.Right,
    2: Direction.Down,
    3: Direction.Left,
};

// Test 2
const [x0, y0, dnum] = [1, 7, 0];
const [xf, yf] = [5, 6];

const robot = new RobotReal(dirMap[dnum], true);
robot.scan();

const dx = xf - x0;
const dy = yf - y0;
console.log(dx, dy);
console.log(robot.localNode);

run_generator(robot.moveToLocalMapNode([robot.yLocal + dy, robot.xLocal + dx]));

brick.display().clear();
brick.display().addLabel('finish', 1, 1);
brick.display().redraw();

delay(15);
