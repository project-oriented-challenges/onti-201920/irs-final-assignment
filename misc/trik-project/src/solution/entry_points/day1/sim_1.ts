import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { RobotSimulator } from '../../robots/RobotSimulator';
import { Command } from '../../Command';
import { Direction } from '../../FieldGraph';
import { run_generator } from '../../helpers';

const INPUT_FILE = 'task1_00.txt';
const robot = new RobotSimulator(Direction.Left); // any direction, but we must specify it
const commands: Command[] = script
    .readAll(INPUT_FILE)[0]
    .trim()
    .split(' ')
    .map((x) => parseInt(x, 10));
console.log(commands);
run_generator(robot.runCommands(commands));

brick.display().clear();
brick.display().addLabel('finish', 1, 1);
brick.display().redraw()
