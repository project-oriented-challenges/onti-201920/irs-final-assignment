import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import mailbox from 'trik_mailbox';
import { Direction, negateDirection, Node } from '../../FieldGraph';
import { RobotReal } from '../../robots/RobotReal';
import { delay } from '../../low_level_control/util/common';
import { run_generator } from '../../helpers';

const FIRST_ROBOT_HULL_NUM = 0;
const FIRST_ROBOT_DIR = Direction.Left;

const SECOND_ROBOT_HULL_NUM = 1;
const SECOND_ROBOT_DIR = Direction.Left;

switch (mailbox.myHullNumber()) {
    case FIRST_ROBOT_HULL_NUM: {
        const robot = new RobotReal(FIRST_ROBOT_DIR);
        robot.scan();
        let relative_dir: Direction;
        let scanned_node: Node;

        for (const cell of robot.walk()) {
            run_generator(robot.moveToLocalMapNode(cell));

            console.log("sending stuff to", SECOND_ROBOT_HULL_NUM);
            mailbox.send(
                SECOND_ROBOT_HULL_NUM,
                JSON.stringify({ type: 'rescan' })
            );
            const msg = JSON.parse(mailbox.receive(true));
            if (msg != null) {
                console.log("WHOOOOP!", msg);
                const { dwall }: { dwall: Direction } = msg;
                relative_dir = negateDirection(dwall);
                scanned_node = robot.localNode;

                break; // from now on we should use optimal localization algorithm
            }
        }

        robot.localize();
        brick.playTone(1000, 100);

        brick.display().clear();
        brick.display().addLabel(`(${robot.xReal},${robot.yReal})`, 1, 1);
        brick.display().addLabel('finish', 1, 20);
        brick.display().redraw();

        let localCell: Node;
        switch (relative_dir!) {
            case Direction.Right:
                localCell = [scanned_node![0], scanned_node![1] + 1];
                break;
            case Direction.Up:
                localCell = [scanned_node![0] - 1, scanned_node![1]];
                break;
            case Direction.Left:
                localCell = [scanned_node![0], scanned_node![1] - 1];
                break;
            case Direction.Down:
                localCell = [scanned_node![0] + 1, scanned_node![1]];
                break;
        }

        mailbox.send(
            SECOND_ROBOT_HULL_NUM,
            JSON.stringify({
                type: 'coords',
                args: { node: localCell!, dx: robot.dx, dy: robot.dy },
            })
        );
        delay(60);

        mailbox.send(
            SECOND_ROBOT_HULL_NUM,
            JSON.stringify({
                type: 'stop',
                args: null,
            })
        );
        break;
    }
    case SECOND_ROBOT_HULL_NUM: {
        /* oh well */
        mailbox.connect("192.168.77.1");

        const robot = new RobotReal(SECOND_ROBOT_DIR);
        robot.scan();
        const walls_initial = robot.get_walls_neighbors();
        let wait = true;

        while (wait) {
            const messageJson = mailbox.receive(true);
            const message: { type: string; args: any } = JSON.parse(
                messageJson
            );
            switch (message.type) {
                case 'rescan': {
                    const walls = robot.get_walls_neighbors();
                    // only one wall can change
                    const newWalls = walls.filter(
                        (x) => !walls_initial.includes(x)
                    );
                    const removedWalls = walls_initial.filter(
                        (x) => !walls.includes(x)
                    );
                    let dwall: Direction | undefined = undefined;
                    if (newWalls.length) {
                        dwall = newWalls[0];
                    } else if (removedWalls.length) {
                        dwall = newWalls[0];
                    }

                    if (dwall !== undefined) {
                        mailbox.send(
                            FIRST_ROBOT_HULL_NUM,
                            JSON.stringify({
                                dwall,
                            })
                        );
                    } else mailbox.send(FIRST_ROBOT_HULL_NUM, "null");
                    break;
                }

                case 'coords': {
                    const {
                        args: { node, dx, dy },
                    }: {
                        args: { node: Node; dx: number; dy: number };
                    } = message;

                    brick.playTone(1000, 100);

                    brick.display().clear();
                    brick
                        .display()
                        .addLabel(`(${node[1] - dx},${node[0] - dy})`, 1, 1);
                    brick.display().addLabel('finish', 1, 20);
                    brick.display().redraw();

                    break;
                }

                case 'stop': {
                    wait = false;
                    break;
                }
            }
        }
        break;
    }
    default: {
        throw new Error(`Unknown hull number: ${mailbox.myHullNumber()}`);
    }
}
