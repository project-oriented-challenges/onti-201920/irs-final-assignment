import '../../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { RobotSimulator } from '../../robots/RobotSimulator';
import { Direction } from '../../FieldGraph';

const INPUT_FILE = 'task1_01.txt';
const dirMap: { [num: number]: Direction } = {
    0: Direction.Up,
    1: Direction.Right,
    2: Direction.Down,
    3: Direction.Left,
};

const lines = script.readAll(INPUT_FILE);
const [dnum] = lines[0]
    .trim()
    .split(' ')
    .map((x) => parseInt(x, 10));
const robot = new RobotSimulator(dirMap[dnum], true);
robot.scan();
robot.localize();

console.log('real map node:', robot.realNode);
brick.display().clear();
brick.display().addLabel(`(${robot.xReal},${robot.yReal})`, 1, 1);
brick.display().redraw()
