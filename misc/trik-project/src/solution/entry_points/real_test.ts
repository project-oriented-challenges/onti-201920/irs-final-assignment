import '../../shim-global';

import brick from 'trik_brick';
import script from 'trik_script';
import { Command } from '../Command';
import { Direction } from '../FieldGraph';
import { RobotReal } from '../robots/RobotReal';
import { run_generator } from '../helpers';

const INPUT_FILE = 'input.txt';
const robot = new RobotReal(Direction.Left, true); // any direction, but we must specify it
// robot.scan();

// console.log(robot.getArtag());

robot.turn(90);
robot.turn(90);
robot.turn(90);
// robot.localize();

// console.log(JSON.stringify(robot.getArtag()));

//brick.display().clear();
//brick.display().addLabel('finish', 1, 1);
//brick.display().redraw();

console.log('finish!');

console.log('__eb3caide9Oojaiku__stop_marker__');
