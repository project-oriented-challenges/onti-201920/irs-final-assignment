import '../../../shim-global';

import brick from 'trik_brick';

import { Direction } from '../../FieldGraph';
import { RobotReal } from '../../robots/RobotReal';
import { delay } from '../../low_level_control/util/common';

const robot = new RobotReal(Direction.Down, false); // any direction
const commands = robot.findArtag();

const ret =
    commands?.join(' ') ??
    (() => {
        throw new Error('ARTag is not found!!1');
    })();


console.log('RET:', ret);

brick.display().clear();
brick.display().addLabel(ret, 1, 1);
brick.display().redraw();

delay(30);
