import '../../../shim-global';

import script from 'trik_script';
import brick from 'trik_brick';

import { artag_decode } from '../../artag/artag_decode';
import { string_to_image, read_artag } from '../../artag/libmorat-ng';
import { RobotSimulator } from '../../robots/RobotSimulator';
import { Direction } from '../../FieldGraph';
import { run_generator } from '../../helpers';

const INPUT_FILE = 'task1_01.txt';

const lines = script.readAll(INPUT_FILE);
const line = lines[0];
const img = string_to_image(line);

const data = read_artag(img);
const commands = artag_decode(data);

const robot = new RobotSimulator(Direction.Down); // any direction
robot.scan();

console.log('Commands:', commands);
run_generator(robot.runCommands(commands));

brick.display().clear();
brick.display().addLabel('finish', 1, 1);
brick.display().redraw()
