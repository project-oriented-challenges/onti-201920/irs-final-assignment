import '../../shim-global';

import script from 'trik_script';
import brick from 'trik_brick';

import { artag_decode } from '../artag/artag_decode';
import { string_to_image, read_artag } from '../artag/libmorat-ng';

const lines = script.readAll('input.txt');
const line = lines[0];
const data = read_artag(string_to_image(line));
const commands = artag_decode(data);

const ret = commands.map(x => String(x)).join(' ');
console.log(ret);
brick.display().clear();
brick.display().addLabel(ret, 1, 1);
brick.display().redraw();
