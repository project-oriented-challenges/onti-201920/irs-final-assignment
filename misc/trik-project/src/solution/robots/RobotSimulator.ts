import brick from 'trik_brick';
import script from 'trik_script';
import { Robot } from './Robot';
import { Command } from '../Command';
import { normalizeAngle } from '../helpers';
import { Direction, WallsState } from '../FieldGraph';

const CELL_LEN = 700; // millimeters
const DIAM = 56;
const CPR = 360;
const CELL_DEG = (CELL_LEN * CPR) / Math.PI / DIAM;

const ROTATION_DELTA = 0.1;
const FORWARD_SPEED = 90;
const MIN_ROTATION_SPEED = 2;
const kGyro = 3;
const SENSORS_DMAX = {
    f: 0, // if set to 1, breaks on speed x4
    b: 0, // if set to 1, breaks on speed x4
    l: 0,
    r: 0,
};

export class RobotSimulator extends Robot {
    private sensors = {
        f: brick.sensor('D1'),
        b: brick.sensor('D2'),
        l: brick.sensor('A2'),
        r: brick.sensor('A1'),
    };

    private motors = {
        l: brick.motor('M4'),
        r: brick.motor('M3'),
    };

    private enc = {
        l: brick.encoder('E4'),
        r: brick.encoder('E3'),
    };

    protected calibrateGyro() {
        console.log('Starting gyroscope calibration...');
        brick.gyroscope().calibrate(2000);
        while (!brick.gyroscope().isCalibrated()) script.wait(1);
        console.log('Calibrated');
    }

    protected getGyroDeg() {
        return -brick.gyroscope().read()[6] / 1000;
    }

    protected stop() {
        this.motors.l.setPower(0);
        this.motors.r.setPower(0);
        script.wait(50);
    }

    protected resetEnc() {
        this.enc.l.reset();
        this.enc.r.reset();
        script.wait(50);
    }

    constructor(direction: Direction, autoScanField: boolean = true) {
        super(direction, autoScanField);
        this.calibrateGyro();
    }

    public _forward(n: number = 1): void {
        this.resetEnc();
        while (
            (this.enc.l.read() + this.enc.r.read()) / 2 < CELL_DEG * n &&
            this.sensors.f.read() > CELL_LEN / 10 / 4
        ) {
            var gyro = normalizeAngle(this.getGyroDeg());
            var predicted_gyro = normalizeAngle(
                (this.direction - this.direction_initial) * 90
            );
            var err = normalizeAngle(predicted_gyro - gyro);

            this.motors.l.setPower(FORWARD_SPEED - err * kGyro);
            this.motors.r.setPower(FORWARD_SPEED + err * kGyro);
            script.wait(1);
        }

        this.stop();
        script.wait(500);
    }
    public turn(deg: number): void {
        this.resetEnc();
        deg = normalizeAngle(deg);
        var delta;

        var currentAngle = normalizeAngle(this.getGyroDeg());
        var requiredAngle = normalizeAngle(currentAngle + deg);

        do {
            delta = normalizeAngle(
                requiredAngle - normalizeAngle(this.getGyroDeg())
            );

            var lv =
                Math.sign(-delta) *
                Math.max(Math.abs(delta * kGyro), MIN_ROTATION_SPEED);
            var rv =
                Math.sign(delta) *
                Math.max(Math.abs(delta * kGyro), MIN_ROTATION_SPEED);
            this.motors.l.setPower(lv);
            this.motors.r.setPower(rv);
            script.wait(1);
        } while (Math.abs(delta) > ROTATION_DELTA);

        this.stop();
        script.wait(500);
    }

    public getWalls(): WallsState {
        const fc = Math.floor(this.sensors.f.read() / (CELL_LEN / 10));
        const lc = Math.floor(this.sensors.l.read() / (CELL_LEN / 10));
        const rc = Math.floor(this.sensors.r.read() / (CELL_LEN / 10));
        const bc = Math.floor(this.sensors.b.read() / (CELL_LEN / 10));

        console.log(
            JSON.stringify({ fc, lc, rc, bc }),
            JSON.stringify(this.localNode),
            this.direction
        );

        const f_wall = fc > SENSORS_DMAX.f || fc <= 0 ? undefined : fc;
        const l_wall = lc > SENSORS_DMAX.l || lc <= 0 ? undefined : lc;
        const r_wall = rc > SENSORS_DMAX.r || rc <= 0 ? undefined : rc;
        const b_wall = bc > SENSORS_DMAX.b || bc <= 0 ? undefined : bc;

        return {
            f_free: fc,
            l_free: lc,
            r_free: rc,
            b_free: bc,

            f_wall,
            l_wall,
            r_wall,
            b_wall,
        };
    }

    public getArtag(): never {
        throw new Error('Method not implemented.');
    }
}
