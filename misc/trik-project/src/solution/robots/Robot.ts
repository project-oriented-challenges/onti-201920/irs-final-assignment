import { Command } from '../Command';
import { normalizeDirection, run_generator } from '../helpers';
import {
    FieldGraph,
    FIELD_SIZE,
    Direction,
    WallsState,
    Node,
    CellState,
} from '../FieldGraph';

export abstract class Robot {
    private readonly verbose = true;
    private localization_data = {
        xmax: 0,
        ymax: 0,
        xmin: FIELD_SIZE,
        ymin: FIELD_SIZE,
    };

    protected readonly direction_initial: Direction;

    constructor(
        public direction: Direction,
        public autoScanField: boolean = true
    ) {
        this.direction_initial = direction;
    }

    protected localMap = new FieldGraph();

    public yLocal = FIELD_SIZE;
    public xLocal = FIELD_SIZE;
    public get localNode(): Node {
        return [this.yLocal, this.xLocal];
    }

    public get realNode(): Node {
        return [this.yReal, this.xReal];
    }

    public dx?: number = undefined;
    public dy?: number = undefined;

    public get located() {
        return this.dx !== undefined && this.dy !== undefined;
    }

    get xReal() {
        if (!this.located) throw new Error('Not located yet');
        return this.xLocal - this.dx!;
    }

    get yReal() {
        if (!this.located) throw new Error('Not located yet');
        return this.yLocal - this.dy!;
    }

    /**
     * Move forward by `n` cells
     */
    protected abstract _forward(n?: number): void;

    /**
     * Move forward by n cells
     * Yields the position after each rescan
     */
    public *forward(n: number = 1): Iterable<Node> {
        if (this.direction === Direction.Up) this.yLocal -= n;
        else if (this.direction === Direction.Down) this.yLocal += n;
        else if (this.direction === Direction.Left) this.xLocal -= n;
        else if (this.direction === Direction.Right) this.xLocal += n;
        this._forward(n);
        if (this.autoScanField) {
            this.scan();
            this.localize_iteration();
        }

        yield this.localNode;
    }

    /**
     * Turn left by `deg` degrees
     */
    public abstract turn(deg: number): void;

    /**
     * Get the number of free cells at each direction:
     *   - `r:` right
     *   - `l:` left
     *   - `f:` forward
     *
     * If counting of free cells is impossible, the method should output
     * whether the wall at the direction exist (0 if exist, 1 otherwise)
     *
     * If you can't say anything about the walls, set the corresponding property to `undefined`
     */
    public abstract getWalls(): WallsState;

    public get_walls_neighbors(): Direction[] {
        const ret: Direction[] = [];
        const w = this.getWalls();
        const ifWall = (x?: number) => x !== undefined && x === 0;
        switch (this.direction) {
            case Direction.Right:
                if (ifWall(w.f_free)) {
                    ret.push(Direction.Right);
                }
                if (ifWall(w.l_free)) {
                    ret.push(Direction.Up);
                }
                if (ifWall(w.b_free)) {
                    ret.push(Direction.Left);
                }
                if (ifWall(w.r_free)) {
                    ret.push(Direction.Down);
                }
                break;
            case Direction.Up:
                if (ifWall(w.r_free)) {
                    ret.push(Direction.Right);
                }
                if (ifWall(w.f_free)) {
                    ret.push(Direction.Up);
                }
                if (ifWall(w.l_free)) {
                    ret.push(Direction.Left);
                }
                if (ifWall(w.b_free)) {
                    ret.push(Direction.Down);
                }
                break;
            case Direction.Left:
                if (ifWall(w.b_free)) {
                    ret.push(Direction.Right);
                }
                if (ifWall(w.r_free)) {
                    ret.push(Direction.Up);
                }
                if (ifWall(w.f_free)) {
                    ret.push(Direction.Left);
                }
                if (ifWall(w.l_free)) {
                    ret.push(Direction.Down);
                }
                break;
            case Direction.Down:
                if (ifWall(w.l_free)) {
                    ret.push(Direction.Right);
                }
                if (ifWall(w.b_free)) {
                    ret.push(Direction.Up);
                }
                if (ifWall(w.r_free)) {
                    ret.push(Direction.Left);
                }
                if (ifWall(w.f_free)) {
                    ret.push(Direction.Down);
                }
                break;
        }

        return ret;
    }

    /**
     * Turns left
     * Yields the position after each rescan
     */
    public *turnLeft(n: number = 1): Iterable<Node> {
        this.turn(90 * n);
        this.direction = normalizeDirection(this.direction + n);
        if (this.autoScanField) {
            this.scan();
            this.localize_iteration();
        }

        yield this.localNode;
    }

    /**
     * Turns right
     * Yields the position after each rescan
     */
    public *turnRight(n: number = 1): Iterable<Node> {
        yield* this.turnLeft(-n);
    }

    public *runCommandsRaw(commands: Command[]): Iterable<Node> {
        for (const command of commands) {
            yield* this.runCommands([command]);
        }
    }

    public *runCommands(commands: Command[]): Iterable<Node> {
        let i = 0;
        while (i < commands.length) {
            switch (commands[i]) {
                case Command.MoveForward:
                    let fwdCounter = 1;
                    i += 1;
                    while (
                        commands[i] == Command.None ||
                        commands[i] == Command.MoveForward
                    ) {
                        if (commands[i] == Command.MoveForward) fwdCounter += 1;
                        i += 1;
                    }

                    yield* this.forward(fwdCounter);
                    break;

                case Command.None:
                    i += 1;
                    break;

                case Command.TurnLeft:
                case Command.TurnRight:
                    let rotCounter = 0;
                    while (
                        commands[i] == Command.TurnLeft ||
                        commands[i] == Command.TurnRight ||
                        commands[i] == Command.None
                    ) {
                        if (commands[i] == Command.TurnLeft) {
                            rotCounter += 1;
                        } else if (commands[i] == Command.TurnRight) {
                            rotCounter -= 1;
                        }
                        i += 1;
                    }

                    yield* this.turnLeft(rotCounter);
                    break;
            }
        }
    }

    public scan(): void {
        for (const node of this.localMap.updateField(
            this.localNode,
            this.getWalls(),
            this.direction
        )) {
            this.localize_iteration(node);
        }

        if (this.verbose) {
            console.log('Map:');
            console.log(this.localMap);
        }
    }

    private localize_iteration([y, x]: Node = this.localNode) {
        let { xmin, xmax, ymin, ymax } = this.localization_data;

        ymin = Math.min(ymin, y);
        ymax = Math.max(ymax, y);
        // if (this.verbose) console.log('ymin, ymax', ymin, ymax);
        xmin = Math.min(xmin, x);
        xmax = Math.max(xmax, x);
        // if (this.verbose) console.log('xmin, xmax', xmin, xmax);

        if (ymax - ymin + 1 === FIELD_SIZE) {
            this.dy = ymin;

            // set the borders of the field when found Y coordinate
            for (let jj = 0; jj < this.localMap.length; ++jj) {
                this.localMap.field[ymin - 1][jj] = CellState.Wall;
                this.localMap.field[ymax + 1][jj] = CellState.Wall;
            }
        }

        if (xmax - xmin + 1 === FIELD_SIZE) {
            this.dx = xmin;

            // set the borders of the field when found X coordinate
            for (let ii = 0; ii < this.localMap.length; ++ii) {
                this.localMap.field[ii][xmin - 1] = CellState.Wall;
                this.localMap.field[ii][xmax + 1] = CellState.Wall;
            }
        }

        this.localization_data = { xmin, xmax, ymin, ymax };
    }

    /**
     * Walk all cells of the local map
     */
    public *walk(): Iterable<Node> {
        yield* this.localMap.dfs(this.localNode);
    }

    public localize(fullMapScan = false): void {
        this.scan();
        for (const [i, j] of this.walk()) {
            const hasUnscannedNeighbor = () =>
                [...this.localMap.neighbors([i, j], true)].reduce(
                    (has, [i, j]) =>
                        has || this.localMap.field[i][j] === CellState.Unknown,
                    false
                );

            if (hasUnscannedNeighbor()) {
                for (const _ of this.moveToLocalMapNode([i, j])) {
                    if (this.located && !fullMapScan) return;
                    if (!hasUnscannedNeighbor()) break;
                }
            }

            if (this.located && !fullMapScan) return;
        }
    }

    private moveByAbsoluteDirectionCommands(
        to: Direction,
        currentDirection: Direction = this.direction
    ): Command[] {
        const diff = normalizeDirection(to - currentDirection);
        const commands: Command[] = [];
        // for (let i = 0; i < diff; ++i) commands.push(Command.TurnLeft);
        switch (diff) {
            case 0:
                break;
            case 1:
                commands.push(Command.TurnLeft);
                break;
            case 2: // Rotate by 180°
                commands.push(Command.TurnLeft, Command.TurnLeft);
                break;
            case 3:
                commands.push(Command.TurnRight);
                break;
        }
        commands.push(Command.MoveForward);

        return commands;
    }

    public *moveByAbsoluteDirection(to: Direction): Iterable<Node> {
        yield* this.runCommands(this.moveByAbsoluteDirectionCommands(to));
    }

    private moveToNeighborNodeCommands(
        to: Node,
        currentPosition: Node,
        currentDirection: Direction = this.direction
    ): Command[] {
        const rel = this.localMap.relativePosition(to, currentPosition);
        if (rel === Direction.Unknown)
            throw new Error("Can't move to node " + JSON.stringify(to));
        return this.moveByAbsoluteDirectionCommands(rel, currentDirection);
    }

    public *moveToNeighborNode(to: Node): Iterable<Node> {
        yield* this.runCommands(
            this.moveToNeighborNodeCommands(to, this.localNode)
        );
    }

    /**
     * Converts path to robot commands
     * @param path The required path. Should start from current node.
     * @param currentDirection
     */
    private pathToCommands(
        path: Node[],
        currentDirection = this.direction
    ): Command[] {
        let commands: Command[] = [];
        let currentPosition = path[0];
        for (const nextNode of path.slice(1)) {
            commands = commands.concat(
                this.moveToNeighborNodeCommands(
                    nextNode,
                    currentPosition,
                    currentDirection
                )
            );
            currentDirection = this.localMap.relativePosition(
                nextNode,
                currentPosition
            );
            currentPosition = nextNode;
        }
        return commands;
    }

    private shortestCommandSetMetric(path: Command[]): number {
        const turns = path.reduce(
            (acc, command) =>
                command === Command.TurnLeft || command === Command.TurnRight
                    ? acc + 1
                    : 0,
            0
        );
        return path.length + turns;
    }

    public getReachableCellNearOccupiedCell(cell: Node): Node | undefined {
        const neighbors = [...this.localMap.neighbors(cell, true)];

        const ret = neighbors.reduce<{ neighbor?: Node; plen: number }>(
            (acc, neighbor) => {
                const { known, path } = this.localMap.bfs_path(
                    this.localNode,
                    neighbor
                );
                if (!known || path === undefined) return acc;

                const plen = this.shortestCommandSetMetric(
                    this.pathToCommands(path)
                );
                if (plen < acc.plen) {
                    return { plen, neighbor };
                }

                return acc;
            },
            { neighbor: undefined, plen: Infinity }
        );

        return ret.neighbor;
    }

    public *moveNearOccupiedCell(cell: Node): Iterable<Node> {
        if (this.verbose) console.log('Moving near:', cell);
        const known_neighbor = this.getReachableCellNearOccupiedCell(cell);
        if (known_neighbor !== undefined) {
            yield* this.moveToLocalMapNode(known_neighbor);
            return;
        }

        const unknown_neighbors = [...this.localMap.neighbors(cell, true)];
        for (const node of unknown_neighbors) {
            console.log('unknown neighbor: ', node);

            try {
                yield* this.moveToLocalMapNode(node);
            } catch (e) {
                if (e instanceof RangeError) {
                } else throw e;
            }

            console.log('localNode: ', this.localNode);

            if (
                this.localMap.relativePosition(this.localNode, cell) !==
                Direction.Unknown
            )
                return;
        }

        throw new Error('Cannot move near cell: ' + JSON.stringify(cell));
    }

    /**
     * Move to the node in local robot map
     */
    public *moveToLocalMapNode(node: Node): Iterable<Node> {
        if (this.verbose) console.log('Moving:', node);
        while (this.yLocal !== node[0] || this.xLocal !== node[1]) {
            const { known, path } = this.localMap.bfs_path(
                this.localNode,
                node
            );

            if (path === undefined) {
                throw new RangeError('Path not found');
            }

            if (!known) {
                const known_path = this.localMap.known_path_part(path);
                const commands = this.pathToCommands(
                    known_path,
                    this.direction
                );

                if (this.verbose)
                    console.log('partial path:', JSON.stringify(known_path));

                yield* this.runCommands(commands);
            } else {
                const commands = this.pathToCommands(path, this.direction);

                if (this.verbose)
                    console.log('fin path:', JSON.stringify(path));

                yield* this.runCommands(commands);
            }
        }
    }

    public findArtag(): Command[] | undefined {
        for (let i = 0; i < 4; ++i) {
            const commands = this.getArtag();
            if (commands !== undefined) return commands;
            run_generator(this.turnLeft());
        }

        return undefined;
    }

    public abstract getArtag(): Command[] | undefined;
}
