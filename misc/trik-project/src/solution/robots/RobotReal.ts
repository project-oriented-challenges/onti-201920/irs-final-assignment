
import ipc_entrypoint from "../low_level_control/movement/index"
import { Robot } from "./Robot";
import { Direction, WallsState } from "../FieldGraph";
import { Command } from "../Command";
import { LLInteractionClient, create_low_level_interation_client } from "../low_level_control/low_level_interaction";

export class RobotReal extends Robot {
    private ipc_client: LLInteractionClient;
    constructor(dir: Direction, auto_scan_field: boolean = true) {
        super(dir, auto_scan_field);
        this.ipc_client = create_low_level_interation_client();
    }

    protected _forward(n: number = 1): void {
        this.ipc_client.execute_forward(n);
        console.log("RobotReal._forward(" + n.toString() + ")");
    }
    public turn(deg: number): void {
        this.ipc_client.execute_turn(deg);
        console.log("RobotReal.turn(" + deg.toString() + ")");
    }
    public getWalls(): WallsState {
        console.log("RobotReal.getWalls()..");
        const r = this.ipc_client.execute_inspect_walls()
        console.log("RobotReal.getWalls():", JSON.stringify(r));
        return r;
    }
    public getArtag(): Command[] | undefined {
        const r = this.ipc_client.execute_scan_artag();
        console.log("RobotReal.getArtag():", JSON.stringify(r.commands));
        return r.commands;
    }
}
