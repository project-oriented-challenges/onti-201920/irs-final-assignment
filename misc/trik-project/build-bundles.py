
import os
import glob
import re
import shutil


name_regex = re.compile(r"^bin/raw/(.*)$")

EP_DIR = "bin/raw/"
REAL_BUNDLES_DIR = "bin/real_bundles/"
SIM_BUNDLES_DIR = "bin/sim_bundles/"
#LL_CONTROL = EP_DIR + "low_level_control.js"
REAL_BUNDLES = [ name_regex.fullmatch(x).group(1) for x in glob.glob(EP_DIR + "**/*") if "real" in x ]
SIM_BUNDLES = [ name_regex.fullmatch(x).group(1) for x in glob.glob(EP_DIR + "**/*") if "sim" in x ]
#[ 'day1/real_1_1', 'day1/real_1_2', 'day1/real_2_1', 'day1/real_2_2', 'day2/day2_cont', 'real_test' ]

def append_end_marker(filename):
    content = open(filename).read()
    content = content + "\nprint('__eb3caide9Oojaiku__stop_marker__');\n"
    with open(filename, 'w') as f:
        f.write(content)

for bundle in REAL_BUNDLES:
    dp = REAL_BUNDLES_DIR + bundle
    os.makedirs(os.path.dirname(dp), exist_ok=True)
    shutil.copy(EP_DIR + bundle, dp)
    append_end_marker(dp)
for bundle in SIM_BUNDLES:
    dp = SIM_BUNDLES_DIR + bundle
    os.makedirs(os.path.dirname(dp), exist_ok=True)
    shutil.copy(EP_DIR + bundle, dp)
