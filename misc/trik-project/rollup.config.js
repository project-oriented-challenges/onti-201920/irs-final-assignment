import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';
//import esformatter from 'rollup-plugin-esformatter'
//import compiler from '@ampproject/rollup-plugin-closure-compiler';
import typescript from 'rollup-plugin-typescript2';
import fs from 'fs';

const warnFilter = /Use of eval is strongly discouraged/;

const extensions = ['.js', '.mjs', '.ts'];

const ENTRY_POINTS_DIR = './src/solution/entry_points';
const ENTRY_POINTS_OUT = 'bin/raw'

function* findEntryPoints(dir) {
    const subfiles = fs.readdirSync(dir, { withFileTypes: true });
    const child_dirs = subfiles
        .filter((x) => x.isDirectory())
        .map((x) => dir + '/' + x.name);
    for (const child_dir of child_dirs) {
        yield* findEntryPoints(child_dir);
    }

    const child_files = subfiles
        .filter((x) => x.name.endsWith('.ts'))
        .map((x) => dir + '/' + x.name)
        .map((x) => x.replace(ENTRY_POINTS_DIR, ''));
    yield* child_files;
}
const entry_points = [...findEntryPoints(ENTRY_POINTS_DIR)]
     .filter(x => x.includes('real_test') || x.includes('day4'));
entry_points.push('/../low_level_control/main.ts');

const external = ['trik_brick', 'trik_script', 'global', 'trik_mailbox', 'trik_threading'];
const globals = {
    'trik_brick': 'brick',
    'trik_script': 'script',
    'global': 'this',
    'trik_mailbox': 'mailbox',
    'trik_threading': 'Threading'
}

const config = [];

config.push(...entry_points.map((x) => ({
    input: ENTRY_POINTS_DIR + x,
    external,
    output: [
        {
            file: ENTRY_POINTS_OUT + x.replace('.ts', '.js'),
            format: 'iife',
            globals
        },
    ],
    plugins: [
        commonjs(),
        //compiler(),
        resolve(),
        json(),
        typescript(),
        babel({
            exclude: 'node_modules/core-js/**',
            extensions: extensions,
        }),
        //esformatter()
    ],
    onwarn: (warning, handeWarning) => {
        // overwite the default warning function
        const str = warning.toString();

        if (warnFilter.test(str)) return;
        handeWarning(warning);
    },
})));


export default config;
